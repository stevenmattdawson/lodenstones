# LodenStones

The game contains a poorly written tutorial explaining the rules. An Interactive Tutorial is planned for future releases.

This repository is meant to show my progress as I turn this proof of concept into a fully fledged board game with network code, AI Opponents, and API integration with Mixer, Twitch, Twitter, and Facebook.

Current State: Version 1.3.

Game rules are finished being tweaked. Lots of Unit Tests have been added. 

## Order of Future Plans: 

1. Unit test all game logic and game piece movement.
2. Add Network code to support playing with people online
3. Add sudo-AI that can provide an interactive tutorial
4. Add API support for Mixer etc.
5. Polish and prepare for Mobile.

## Going forward.

The goal is clean code and test driven development. Unit tests must be in place before refactoring can begin, and should be in place before any more new code is added.