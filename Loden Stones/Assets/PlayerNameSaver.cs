﻿using Assets.GameTokens;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameSaver : MonoBehaviour
{
	public Text PlayerNameTextBox;
	public InputField input;
	public string PlayerName {
		get => PlayerNameTextBox.text;
		set => PlayerNameTextBox.text = value;
	}
	public PlayerNum PlayerNumber;


	// Start is called before the first frame update
	private void Start() {
		input.text = PlayerNumber == PlayerNum.Player1 ? ScenePersistantInfo.Info.Player1Name : ScenePersistantInfo.Info.Player2Name;
	}

	public void SavePlayerName() {
		if (PlayerNumber == PlayerNum.Player1) {
			ScenePersistantInfo.Info.Player1Name = PlayerName;
			Debug.Log($"{PlayerNumber.ToString()} was set to {ScenePersistantInfo.Info?.Player1Name}");
		}
		else {
			ScenePersistantInfo.Info.Player2Name = PlayerName;
			Debug.Log($"{PlayerNumber.ToString()} was set to {ScenePersistantInfo.Info?.Player2Name}");

		}
	}
}
