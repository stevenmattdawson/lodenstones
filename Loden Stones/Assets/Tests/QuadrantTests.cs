﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets;
using Assets.GameTokens;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class QuadrantTests
    {
		GameBoard Board;
		GameManager Game;
		List<Stone> Player1Stones;
		List<Stone> Player2Stones;
		List<Well> LodenWells;


		[OneTimeSetUp]
		public void SetUpForTest() {
			TestingTools.OneTimeSetUp(ref Board, ref Game, ref Player1Stones, ref Player2Stones, ref LodenWells);
		}


		[SetUp]
		public void Setup() {
			TestingTools.SetUp(ref Game);
		}



		[OneTimeTearDown]
		public void TearDownForTest() {
			TestingTools.OneTimeTearDown(ref Board);
		}

		// A Test behaves as an ordinary method
		[Test]
        public void _GetQuadrantTest()
        {
			try {
				Assert.True(Board.GetQuadrant((0, 0)) == Quadrant.LowerLeft);
				Assert.True(Board.GetQuadrant((5, 0)) == Quadrant.LowerRight);
				Assert.True(Board.GetQuadrant((0, 5)) == Quadrant.UpperLeft);
				Assert.True(Board.GetQuadrant((5, 5)) == Quadrant.UpperRight);
				Assert.True(Board.GetQuadrant((-1, 0)) == Quadrant.None);
			}
			catch(Exception ex) {
				Debug.Log(ex.Message + ex.StackTrace);
				Assert.Fail();
			}
		}

		[Test]
		public void _GetIsInQuadrantTest() {
			Assert.True(Board.IsInQuadrant(Quadrant.LowerLeft, (0, 0)));
			Assert.True(Board.IsInQuadrant(Quadrant.LowerRight, (5, 0)));
			Assert.True(Board.IsInQuadrant(Quadrant.UpperLeft, (0, 5)));
			Assert.True(Board.IsInQuadrant(Quadrant.UpperRight, (5, 5)));
			Assert.False(Board.IsInQuadrant(Quadrant.LowerLeft, (-1, 0)));
		}

		[Test]
		public void _SelectQuadrantTest() {
			Board.InstantlyMoveStoneToCell(Player1Stones[0], (0, 0));
			Board.SelectQuadrant(Player1Stones[0]);
			Assert.IsTrue(Board.LowerLeft.activeSelf);
			Assert.IsFalse(Board.LowerRight.activeSelf);
			Assert.IsFalse(Board.UpperLeft.activeSelf);
			Assert.IsFalse(Board.UpperRight.activeSelf);
			Board.InstantlyMoveStoneToCell(Player1Stones[1], (5, 0));
			Board.SelectQuadrant(Player1Stones[1]);
			Assert.IsFalse(Board.LowerLeft.activeSelf);
			Assert.IsTrue(Board.LowerRight.activeSelf);
			Assert.IsFalse(Board.UpperLeft.activeSelf);
			Assert.IsFalse(Board.UpperRight.activeSelf);
			Board.InstantlyMoveStoneToCell(Player1Stones[2], (0, 5));
			Board.SelectQuadrant(Player1Stones[2]);
			Assert.IsFalse(Board.LowerLeft.activeSelf);
			Assert.IsFalse(Board.LowerRight.activeSelf);
			Assert.IsTrue(Board.UpperLeft.activeSelf);
			Assert.IsFalse(Board.UpperRight.activeSelf);
			Board.InstantlyMoveStoneToCell(Player1Stones[3], (5, 5));
			Board.SelectQuadrant(Player1Stones[3]);
			Assert.IsFalse(Board.LowerLeft.activeSelf);
			Assert.IsFalse(Board.LowerRight.activeSelf);
			Assert.IsFalse(Board.UpperLeft.activeSelf);
			Assert.IsTrue(Board.UpperRight.activeSelf);
		}

		[Test]
		public void _ResetQuadrantTest() {
			Board.InstantlyMoveWellToCellFromTile(LodenWells[0], (-2, 5), Board.Tiles[LodenWells[0].X, LodenWells[0].Y]);
			Board.InstantlyMoveWellToCellFromTile(LodenWells[2], (-2, 4), Board.Tiles[LodenWells[2].X, LodenWells[2].Y]);
			Board.ResetQuadrants();
			Assert.IsFalse(Board.LowerLeft.activeSelf);
			Assert.IsTrue(Board.LowerRight.activeSelf);
			Assert.IsTrue(Board.UpperLeft.activeSelf);
			Assert.IsFalse(Board.UpperRight.activeSelf);
		}

		[Test]
		public void _GetStonesInQuadrantsTest() {
			Board.InstantlyMoveStoneToTile(Player1Stones[0], Board.Tiles[ 0, 0]);
			Board.InstantlyMoveStoneToTile(Player1Stones[1], Board.Tiles[1, 1]);
			Board.InstantlyMoveStoneToTile(Player1Stones[2], Board.Tiles[2, 2]);
			Board.InstantlyMoveStoneToTile(Player1Stones[3], Board.Tiles[2, 0]);
			var stones = Board.GetStonesInQuadrant(Quadrant.LowerLeft);
			Assert.IsTrue(stones.Count == 4, $"stone Count = {stones.Count} and should have been {4}");
		}

		[Test]
		public void _GetTilesInQuadrantTest() {
			var lowerLeft = Board.GetTilesInQuadrant(Quadrant.LowerLeft);
			var lowerRight = Board.GetTilesInQuadrant(Quadrant.LowerRight);
			var upperRight = Board.GetTilesInQuadrant(Quadrant.UpperRight);
			var upperLeft = Board.GetTilesInQuadrant(Quadrant.UpperLeft);

			Assert.IsTrue(lowerLeft.Count == 9);
			Assert.IsTrue(lowerRight.Count == 9);
			Assert.IsTrue(upperRight.Count == 9);
			Assert.IsTrue(upperLeft.Count == 9);

			Assert.IsTrue(lowerLeft.Any(x => x.X == 0 && x.Y == 0));
			Assert.IsTrue(lowerRight.Any(x => x.X == 5 && x.Y == 0));
			Assert.IsTrue(upperLeft.Any(x => x.X == 0 && x.Y == 5));
			Assert.IsTrue(upperRight.Any(x => x.X == 5 && x.Y == 5));
		}

	}
}
