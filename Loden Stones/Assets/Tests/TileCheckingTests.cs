﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets;
using Assets.GameTokens;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TileCheckingTests
    {
		GameBoard Board;
		GameManager Game;
		List<Stone> Player1Stones;
		List<Stone> Player2Stones;
		List<Well> LodenWells;

		[OneTimeSetUp]
		public void SetUpForTest() {
			TestingTools.OneTimeSetUp(ref Board, ref Game, ref Player1Stones, ref Player2Stones, ref LodenWells);
		}

		[SetUp]
		public void Setup() {
			TestingTools.SetUp(ref Game);
		}

		[OneTimeTearDown]
		public void TearDownForTest() {
			TestingTools.OneTimeTearDown(ref Board);
		}

		[Test]
		public void _OnGameBoardTest() {
			Assert.True(GameBoard.OnGameBoard((0, 0)), "(0,0) was not on the gameboard");
			Assert.True(GameBoard.OnGameBoard((GameBoard.LogicalBoardSize, 0)), "(5,0) was not ont the gameboard");
			Assert.True(GameBoard.OnGameBoard((GameBoard.LogicalBoardSize, GameBoard.LogicalBoardSize)), "(5,0) was not ont the gameboard");
			Assert.True(GameBoard.OnGameBoard((0, GameBoard.LogicalBoardSize)), "(5,0) was not ont the gameboard");

			Assert.False(GameBoard.OnGameBoard((-1, 0)), "(-1, 0) was on the gameboard");
			Assert.False(GameBoard.OnGameBoard((0, GameBoard.LogicalBoardSize+1)), "(0, 6) was on the gameboard");
			Assert.False(GameBoard.OnGameBoard((GameBoard.LogicalBoardSize+1, GameBoard.LogicalBoardSize+1)), "(6,6) was on the gameboard");
			Assert.False(GameBoard.OnGameBoard((GameBoard.LogicalBoardSize+1, 0)), "(6, 0) was on the gameboard");
			Assert.False(GameBoard.OnGameBoard((0, -1)), "(0, -1) was on the gameboard");
		}

		[Test]
		public void _IsWellNearTest() {
			Assert.True(Board.IsWellNear(Board.Tiles[0, 0]));
			Board.InstantlyMoveWellToTileFromTile(Board.Tiles[1,1].Well, Board.Tiles[2, 2], Board.Tiles[1,1]);
			Assert.False(Board.IsWellNear(Board.Tiles[0, 0]), "Well should not be near (0, 0)");
			Assert.False(Board.IsWellNear(Board.Tiles[1, 0]), "Well should not be near (1, 0)");
			Assert.False(Board.IsWellNear(Board.Tiles[2, 0]), "Well should not be near (2, 0)");
			Assert.False(Board.IsWellNear(Board.Tiles[0, 1]), "Well should not be near (0, 1)");
			Assert.False(Board.IsWellNear(Board.Tiles[0, 2]), "Well should not be near (0, 2)"); 
		}

		[Test]
		public void _IndicateWellNearTest() {
			Board.InstantlyMoveWellToTileFromTile(Board.Tiles[1,1].Well, Board.Tiles[2, 2], Board.Tiles[1,1]);
			Board.IndicateWellNear();
			Assert.True(Board.Tiles[0, 0].ColorIndicator.color == BoardTile.InvalidColor);
			Assert.True(Board.Tiles[1, 0].ColorIndicator.color == BoardTile.InvalidColor);
			Assert.True(Board.Tiles[2, 0].ColorIndicator.color == BoardTile.InvalidColor);
			Assert.True(Board.Tiles[0, 1].ColorIndicator.color == BoardTile.InvalidColor);
			Assert.True(Board.Tiles[0, 2].ColorIndicator.color == BoardTile.InvalidColor);
		}

		[Test]
		public void _IndicatePulseOptionsTest() {
			Board.InstantlyMoveStoneToTile(Player1Stones[0], Board.Tiles[0, 0]);
			Board.InstantlyMoveWellToTileFromTile(Board.Tiles[1, 4].Well, Board.Tiles[1, 0], Board.Tiles[1, 4]);

			Board.IndicatePulseOption(Player1Stones[0]);
			Assert.True(Board.Tiles[1, 0].Well.GetComponent<SpriteRenderer>().color == Board.Tiles[1, 0].Well.InPulseRange);
			Assert.True(Board.Tiles[1, 1].Well.GetComponent<SpriteRenderer>().color == Board.Tiles[1, 1].Well.InPulseRange);
		}

    }
}
