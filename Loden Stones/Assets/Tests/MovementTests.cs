﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using Assets.GameTokens;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;

namespace Tests
{
    public class MovementTests 
    {

		GameBoard board;
		Vector3Int targetLocation = new Vector3Int(3, 3, 0);
		(int x, int y) targetCell = (3, 3);



		[OneTimeSetUp]
		public void SetUpForTest() {
			var obj = Resources.Load<GameObject>("LodenBoard");
			obj = Object.Instantiate(obj);
			board = obj.GetComponent<GameBoard>();
			board.Construct();
		}

		[OneTimeTearDown]
		public void TearDownForTest() {
			GameObject.Destroy(board);
			board = null;
		}

  

		private static void MovementSucceededAsserts(GameObject obj, Vector3 startPosition, Vector3 endGoal) {
			Assert.AreNotEqual(startPosition, obj.transform.position);
			Debug.Log($"_MoveObjectToCellTest endPosition: {obj.transform.position.ToString()} endGoal: {endGoal}");
			Assert.True(obj.transform.position.x == endGoal.x && obj.transform.position.y == endGoal.y);
		}
		#region Stone Movement
		// A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
		// `yield return null;` to skip a frame.
		[UnityTest]
        public IEnumerator _MoveObjectToCellTest() {
			var obj = new GameObject();
			obj.transform.position = new Vector3();
			var startPosition = obj.transform.position;
			var endGoal = board.Board.GetCellCenterWorld(targetLocation);
			yield return board.MoveObjectToCell(obj, targetCell);
			MovementSucceededAsserts(obj, startPosition, endGoal);

		}

		[UnityTest]
        public IEnumerator _MoveStoneToCellTest()
        {
			/** TODO: there is a current problem where we are using the same 
			  *	game for every test, so that state is not always a new unplayed with board.
			  * need to figure out how to make sure each test starts from the same location.
			  * Until then, each test will just use a different stone. I'll solve this problem after i've written 16 tests.
			**/
			var stone = board.Game.Player1Stones[0]; 
			var startPosition = stone.transform.position;
			var endGoal = board.Board.GetCellCenterWorld(targetLocation);
			//test everything that Moving a stone to a cell should accomplish;
			board.SelectedStone = stone;
			stone.IsSelected = true;
			yield return board.MoveStoneToCell(stone, targetCell);
			MovementSucceededAsserts(stone.gameObject, startPosition, endGoal);
			Assert.False(stone.IsSelected);
			Assert.True(board.SelectedStone == null);
			Assert.True(targetCell.x == stone.X);
			Assert.True(targetCell.y == stone.Y);
		}

		[UnityTest]
		public IEnumerator _MoveStoneToTileTest() {
			var stone = board.Game.Player1Stones[1];
			var tile = board.Tiles[3, 3];
			var startPosition = stone.transform.position;
			var endGoal = board.Board.GetCellCenterWorld(targetLocation);
			//test everything that Moving a stone to a tile should accomplish;
			board.SelectedStone = stone;
			stone.IsSelected = true;
			tile.IsSelected = true;
			Assert.True(tile.HasStone == false);
			Assert.True(tile.Stone == null);
			board.SelectedTile = tile;
			
			//TODO: eventualy test a stone being activated or deactivate based on a well present in the quadrant


			yield return board.MoveStoneToTile(stone, tile);
			MovementSucceededAsserts(stone.gameObject, startPosition, endGoal);

			Assert.False(stone.IsSelected);
			Assert.False(tile.IsSelected);
			Assert.True(board.SelectedStone == null);
			Assert.True(board.SelectedTile == null);
			Assert.True(tile.HasStone == true);
			Assert.True(tile.Stone == stone);
			Assert.True(tile.X == stone.X);
			Assert.True(tile.Y == stone.Y);
		}


		[UnityTest]
		public IEnumerator _MoveStoneToTileFromTileTest() {
			var stone = board.Game.Player1Stones[2];
			var endTile = board.Tiles[3, 4];
			var startTile = board.Tiles[0, 0];
			yield return board.MoveStoneToTile(stone, startTile);
			Assert.True(startTile.HasStone); // ensures test is actually going to test what we want to test
			Assert.True(startTile.Stone == stone);
			var startPosition = stone.transform.position;
			var endGoal = board.Board.GetCellCenterWorld(new Vector3Int(3, 4,0));

			//test everything that Moving a stone to a tile should accomplish;
			board.SelectedStone = stone;
			stone.IsSelected = true;
			endTile.IsSelected = true;
			Assert.True(endTile.HasStone == false);
			Assert.True(endTile.Stone == null);
			board.SelectedTile = endTile;

			//TODO: eventualy test a stone being activated or deactivate based on a well present in the end quadrant

			yield return board.MoveStoneToTileFromTile(stone, endTile, startTile);
			MovementSucceededAsserts(stone.gameObject, startPosition, endGoal);

			Assert.False(stone.IsSelected);
			Assert.False(endTile.IsSelected);
			Assert.False(startTile.IsSelected); 
			Assert.False(startTile.HasStone);

			Assert.True(endTile.HasStone);
			Assert.True(endTile.Stone == stone);
			Assert.True(board.SelectedStone == null);
			Assert.True(board.SelectedTile == null);
			Assert.True(endTile.X == stone.X);
			Assert.True(endTile.Y == stone.Y);
		}

		[UnityTest]
		public IEnumerator _MoveStoneToCellFromTileTest() {
			var stone = board.Game.Player1Stones[2];
			
			var startTile = board.Tiles[0, 0];
			yield return board.MoveStoneToTile(stone, startTile);
			Assert.True(startTile.HasStone); // ensures test is actually going to test what we want to test
			Assert.True(startTile.Stone == stone);
			var startPosition = stone.transform.position;
			var endGoal = board.Board.GetCellCenterWorld(new Vector3Int(stone.StartingCoordinates.x, stone.StartingCoordinates.y, 0));

			//TODO: eventualy test a stone being activated or deactivate based on a well present in the end quadrant

			yield return board.MoveStoneToCellFromTile(stone, stone.StartingCoordinates, startTile);
			MovementSucceededAsserts(stone.gameObject, startPosition, endGoal);

			Assert.False(stone.IsSelected);
			Assert.False(startTile.IsSelected);
			Assert.False(startTile.HasStone);



			Assert.True(stone.StartingCoordinates.x == stone.X);
			Assert.True(stone.StartingCoordinates.y == stone.Y);
		}

		[UnityTest]
		public IEnumerator _SwapStoneLocationTest() {
			var stone1 = board.Game.Player1Stones[1];
			var stone2 = board.Game.Player1Stones[2];

			if (stone1.InPlay == false) {
				yield return board.MoveStoneToTile(stone1, board.Tiles[0, 0]);
			}
			if (stone2.InPlay == false) {
				yield return board.MoveStoneToTile(stone2, board.Tiles[2, 2]);
			}

			var tile1 = board.Tiles[stone1.X, stone1.Y];
			var tile2 = board.Tiles[stone2.X, stone2.Y];

			yield return board.SwapStoneLocations(stone1, stone2);

			Assert.True(tile1.X == stone2.X);
			Assert.True(tile1.X == stone2.Y);
			Assert.True(tile2.X == stone1.X);
			Assert.True(tile2.Y == stone1.Y);
			
		}
		#endregion


		#region Well Movement
		[UnityTest]
		public IEnumerator _MoveWellToCellTest() {
			var well = board.Game.LodenWells[0];
			var startPosition = well.transform.position;
			var endGoal = board.Board.GetCellCenterWorld(targetLocation);
			var startCell = well.Coordinates;
			//test everything that Moving a stone to a cell should accomplish;

			yield return board.MoveWellToCell(well, targetCell);
			MovementSucceededAsserts(well.gameObject, startPosition, endGoal);
			Assert.True(targetCell.x == well.X);
			Assert.True(targetCell.y == well.Y);

			startPosition = well.transform.position;
			endGoal = board.Board.GetCellCenterWorld(new Vector3Int(startCell.x, startCell.y, 0));
			yield return board.MoveWellToCell(well, startCell);
			MovementSucceededAsserts(well.gameObject, startPosition, endGoal);
			Assert.True(startCell.x == well.X);
			Assert.True(startCell.y == well.Y);
		}

		[UnityTest]
		public IEnumerator _MoveWellToTileTest() {

			//unit tests won't run if this function fails, so rather than test the motion itself, 
			//i'm just going to test the state changes that need to occur when this function is called
			var well = board.Game.LodenWells[0];
			Debug.Log($"-T- Well at X: {well.X} and Y: {well.Y}");
			var tile = board.Tiles[3,3];
			Assert.True(tile.HasWell == false);

			var cleanUpTile = board.Tiles[well.X, well.Y];

			Quadrant quad = board.GetQuadrant(tile.Coordinates);
			int numQuadrants = board.WellsByQuadrant[quad];

			yield return board.MoveWellToTile(well, tile);
			Debug.Log($"-T- Well at X: {well.X} and Y: {well.Y}"); 
			Assert.True(tile.HasWell == true);
			Assert.True(tile.Well == well);

			cleanUpTile.HasWell = false;
			cleanUpTile.Well = null;
			board.WellsByQuadrant[board.GetQuadrant(cleanUpTile.Coordinates)]--;

			Assert.True(board.WellsByQuadrant[quad] == numQuadrants + 1);
			Assert.True(board.GetQuadrant(quad).activeSelf);

			var stones = board.GetStonesInQuadrant(quad);
			foreach (var stone in stones) {
				Assert.True(stone.ShownAsActive);
			}
		}

		[UnityTest]
		public IEnumerator _MoveWellToTileFromTileTest() {

			//unit tests won't run if this function fails, so rather than test the motion itself, 
			//i'm just going to test the state changes that need to occur when this function is called
			var well = board.Game.LodenWells[3];
			Debug.Log($"-T- Well at X: {well.X} and Y: {well.Y}");
			var startTile = board.Tiles[well.X, well.Y];
			var endTile = board.Tiles[2,2];
			Assert.True(startTile.HasWell == true);
			Assert.True(startTile.Well != null);
			Quadrant startQuad = board.GetQuadrant(startTile.Coordinates);
			Quadrant endQuad = board.GetQuadrant(endTile.Coordinates);
			int numStartQuadrant = board.WellsByQuadrant[startQuad];
			int numEndQuadrant = board.WellsByQuadrant[endQuad];

			yield return board.MoveWellToTileFromTile(well, endTile, startTile);
			Assert.True(board.WellsByQuadrant[startQuad] == numStartQuadrant - 1);
			Assert.True(board.WellsByQuadrant[endQuad] == numEndQuadrant + 1);

			Debug.Log($"-T- Well at X: {well.X} and Y: {well.Y}");
			Assert.True(startTile.HasWell == false);
			Assert.True(startTile.Well == null);
			Assert.True(endTile.HasWell == true);
			Assert.True(endTile.Well != null);

			Assert.True(board.GetQuadrant(endQuad).activeSelf);
			Assert.False(board.GetQuadrant(startQuad).activeSelf);

			var stones = board.GetStonesInQuadrant(startQuad);
			foreach (var stone in stones) {
				Assert.True(stone.ShownAsActive == false);
			}
			stones = board.GetStonesInQuadrant(endQuad);
			foreach (var stone in stones) {
				Assert.True(stone.ShownAsActive);
			}
		}

		[UnityTest]
		public IEnumerator _ResetWellsIfAllWellsRemovedTest() {
			var LodenWells = board.Game.LodenWells;
			var stone = board.Game.Player2Stones[0];
			var well1 = LodenWells[0];
			var well2 = LodenWells[1];
			var well3 = LodenWells[2];
			var well4 = LodenWells[3];

			var startTile1 = board.Tiles[well1.X, well1.Y];
			var startTile2 = board.Tiles[well2.X, well2.Y];
			var startTile3 = board.Tiles[well3.X, well3.Y];
			var startTile4 = board.Tiles[well4.X, well4.Y];

			board.StartCoroutine(board.MoveWellToTileFromTile(well1, board.Tiles[0,0], startTile1));
			board.StartCoroutine(board.MoveWellToTileFromTile(well2, board.Tiles[2,0], startTile2));
			board.StartCoroutine(board.MoveWellToTileFromTile(well3, board.Tiles[0,2], startTile3));
			yield return board.MoveWellToTileFromTile(well4, board.Tiles[2,2], startTile4);

			//this asserts that we've set the board up in the correct way.
			Assert.True(board.QuadrantHasWell(Quadrant.LowerLeft));
			Assert.False(board.QuadrantHasWell(Quadrant.LowerRight));
			Assert.False(board.QuadrantHasWell(Quadrant.UpperLeft));
			Assert.False(board.QuadrantHasWell(Quadrant.UpperRight));

			board.StartCoroutine(board.MoveWellToStoneThenCellFromTile(well1, stone, well1.StartingCoordinates, startTile1));
			board.StartCoroutine(board.MoveWellToStoneThenCellFromTile(well2, stone, well2.StartingCoordinates, startTile2));
			board.StartCoroutine(board.MoveWellToStoneThenCellFromTile(well3, stone, well3.StartingCoordinates, startTile3));
			yield return board.MoveWellToStoneThenCellFromTile(well4, stone, well4.StartingCoordinates, startTile4);
			

			string outputCoor = "";
			foreach (var well in LodenWells) {
				outputCoor += $"{well.ToString()} - ";
			}
			Debug.Log(board.MoveWellToStoneThenCellFromTileIsRunning + " " + outputCoor);

			foreach (var numWells in board.WellsByQuadrant) {
				Debug.Log($"{numWells.Key} = {numWells.Value}");
			}



			Assert.True(LodenWells.Any(x => x.Coordinates == (1, 1)));
			Assert.True(LodenWells.Any(x => x.Coordinates == (1, 4)));
			Assert.True(LodenWells.Any(x => x.Coordinates == (4, 4)));
			Assert.True(LodenWells.Any(x => x.Coordinates == (4, 1)));

		}

			#endregion
	}
}
