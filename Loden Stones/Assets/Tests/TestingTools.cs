﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets;
using Assets.GameTokens;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public static class TestingTools
    {

		public static void OneTimeSetUp(ref GameBoard board, ref GameManager game, ref List<Stone> player1Stones, ref List<Stone> player2Stones, ref List<Well> lodenWells) {
			try {
				var obj = Resources.Load<GameObject>("LodenBoard");
				obj = UnityEngine.Object.Instantiate(obj);
				board = obj.GetComponent<GameBoard>();
				board.Start();
				game = board.Game;
				game.Start();
				player1Stones = game.Player1Stones;
				player2Stones = game.Player2Stones;
				lodenWells = game.LodenWells;
			}
			catch (Exception ex) {
				Debug.Log(ex.Message + ex.StackTrace);
				Assert.Fail();
			}
		}

		public static void SetUp(ref GameManager game) {
			try {
				game.ResetGame();
			}
			catch (Exception ex) {
				Debug.Log(ex.Message + ex.StackTrace);
				Assert.Fail();
			}
		}

		public static void OneTimeTearDown(ref GameBoard board) {
			GameObject.Destroy(board);
			board = null;
		}

	}
}
