﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets;
using Assets.GameTokens;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace Tests
{
    public class GameLogicTests
    {

		GameBoard Board;
		GameManager Game;
		List<Stone> Player1Stones;
		List<Stone> Player2Stones;
		List<Well> LodenWells;

		[OneTimeSetUp]
		public void SetUpForTest() {
			TestingTools.OneTimeSetUp(ref Board, ref Game, ref Player1Stones, ref Player2Stones, ref LodenWells);
		}

		[SetUp]
		public void Setup() {
			TestingTools.SetUp(ref Game);
		}

		[OneTimeTearDown]
		public void TearDownForTest() {
			TestingTools.OneTimeTearDown(ref Board);
		}

		// A Test behaves as an ordinary method
		[Test]
        public void _Player1WinsTest()
        {
            // Use the Assert class to test conditions
        }

		[Test]
		public void _Player2WinsTest() {

		}

		[Test]
		public void _SkipCausesWinTest() {

		}

		[Test]
		public void _ProgressTurnAdvancesTurnTest() {

		}

		#region TileClicked

		[Test]
		public void _TileClickedMovesWellAndStoneTest() {

		}

		[Test]
		public void _TileClickedMovesStoneFromReserveTest() {

		}

		[Test]
		public void _TileClickedMovesStoneOnBoardTest() {

		}

		[Test]
		public void _TileClickedDoesnMoveInvalidClickTest() {

		}

		[Test]
		public void _TileSelectionIsInvalidTest() {

		}
		#endregion

		#region stoneClicked
		[Test]
		public void _StoneClickedUnselectedTest() {

		}

		[Test]
		public void _StoneClickedCausesSwapTest() {

		}

		[Test]
		public void _StoneClickedShowArrowsTest() {

		}

		[Test]
		public void _StoneClickedShowWellPulseTEst() {

		}
		#endregion

		#region AttackButton
		[Test]
		public void _AttackButtonCausesStreamTest() {

		}

		[Test]
		public void _AttackButtonCuasesPulseTest() {

		}

		[Test]
		public void _AttackButtonInvalidTest() {

		}
		#endregion


	}
}
