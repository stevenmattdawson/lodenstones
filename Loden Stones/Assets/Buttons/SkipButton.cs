﻿using UnityEngine;

namespace Assets.Buttons {
	public class SkipButton : MonoBehaviour
	{
		public void Clicked() {
			GameManager.Game.SkipButtonClicked();
		}
	}
}
