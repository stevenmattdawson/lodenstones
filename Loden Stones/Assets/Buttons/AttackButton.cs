﻿using UnityEngine;

namespace Assets.Buttons {
	public class AttackButton : MonoBehaviour {
		public void Clicked() {
			GameManager.Game.AttackButtonClicked();
		}

	}
}
