﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{

	private void OnCollisionEnter2D(Collision2D collision) {
		Destroy(this);
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		Destroy(this);
		
	}
}
