﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideStonesSaver : MonoBehaviour
{
	public Toggle HasStonesToggle;


	private void Awake() {
		
	}

	private void Start() {
		HasStonesToggle.isOn = ScenePersistantInfo.Info?.HideStones == true;
	}
	public void SaveHideStones() {
		ScenePersistantInfo.Info.HideStones = HasStonesToggle.isOn;
	}
}
