﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenePersistantInfo : MonoBehaviour
{

	public static ScenePersistantInfo Info;

	public string Player1Name = "Player 1";
	public string Player2Name = "Player 2";
	public bool HideStones = false;
	
	void Awake() {
		if (Info == null) {
			DontDestroyOnLoad(gameObject);
			Info = this;
		}
		else if (Info != this) {
			Destroy(gameObject); // any scene persistant info that isn't the singleton will destroy itself.
		}
	}

	// Start is called before the first frame update
	void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
		//I'll find a better home for this eventually, but for now, this object is persistant and this will 
		//provide a way to close the application if playing full screen.
		if (Input.GetKey("escape")) {
			Application.Quit();
		}
	}
}
