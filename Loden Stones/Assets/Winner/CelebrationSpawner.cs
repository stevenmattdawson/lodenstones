﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelebrationSpawner : MonoBehaviour
{
	public Transform EndLocation;
	public Vector2 ShootDirection;
	public GameObject Ammo;

	private Vector3 startingLocation;
	private Vector3 endingLocation;
    // Start is called before the first frame update
    void Start()
    {
		startingLocation = transform.position;
		endingLocation = EndLocation.position;
		StartCoroutine(Shoot());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	IEnumerator Shoot() {
		while(true) {
			var bullet = Instantiate(Ammo, transform.position, new Quaternion(0, 0, 0, 1));
			bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(ShootDirection.x * 10, ShootDirection.y * 10);
			yield return new WaitForSeconds(1);
		}

		
	}
}
