﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RainbowEffect : MonoBehaviour
{
	Text Winner;


	int r = 255;
	int g = 255;
	int b = 255;

    // Start is called before the first frame update
    void Start()
    {
		Winner = GetComponentInChildren<Text>();
		StartCoroutine(Colorize());
	}

    // Update is called once per frame
    void Update()
    {
	


    }

	IEnumerator Colorize() {
		while (true) {
			CyclicalIncrement();
			Winner.color = new Color(r, g, b);
			Debug.Log($"Winner.color.r = {Winner.color.r} and R = {r}");
			yield return new WaitForSeconds(.02f);
		}
		
	}

	private void CyclicalIncrement() {
		if (r != 255) {
			r++;
		}
		else if (g != 255) {
			g++;
		}
		else if (b != 255) {
			b++;
		}
		else {
			r = b = g = 0;
		}
	
	}

	private void Randomize() {
		r = Random.Range(0, 255);
		g = Random.Range(0, 255);
		b = Random.Range(0, 255);
	}
}
