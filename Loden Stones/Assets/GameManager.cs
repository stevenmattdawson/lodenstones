﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using Assets.GameTokens;
using Assets;

public class GameManager : MonoBehaviour {
	public bool Debugging = false;

	public static GameManager Game;

	public PlayerNum Turn = PlayerNum.Player1;

	private Color Valid = Color.green;
	private Color Invalid = Color.red;

	private GameBoard _board;
	public GameBoard Board {
		get {
			if (_board == null) {
				_board = this.gameObject.transform.parent.GetComponent<GameBoard>();
			}
			return _board;
		}
	}

	public AudioClip CriticalError;
	public AudioClip CantTouchThis;
	public AudioClip Error;
	public AudioClip Stream;
	public AudioClip Pulse;
	public AudioSource Sounds;

	private bool EnableAttacks = false;

	public Dictionary<int, bool> RemovedStonesPairsPlayer1 = new Dictionary<int, bool>();
	public Dictionary<int, bool> RemovedStonesPairsPlayer2 = new Dictionary<int, bool>();
	public List<Stone> RemovedStones;
	public List<Well> RemovedWells = new List<Well>();
	public List<Stone> Player1Stones;
	public List<Stone> Player2Stones;
	public List<Stone> ActiveStones => Turn == PlayerNum.Player1 ? Player1Stones : Player2Stones;

	public List<Well> Player1Wells;
	public List<Well> Player2Wells;
	public List<Well> ActiveWells {
		get => Turn == PlayerNum.Player1 ? Player1Wells : Player2Wells;
		set {
			if (Turn == PlayerNum.Player1) {
				Player1Wells = value;
			}
			else {
				Player2Wells = value;
			}
		}
	}
	public List<Well> InActiveWells {
		get => Turn == PlayerNum.Player1 ? Player2Wells : Player1Wells;
		set {
			if (Turn == PlayerNum.Player1) {
				Player2Wells = value;
			}
			else {
				Player1Wells = value;
			}
		}
	}


	public List<Well> LodenWells;

	private const int MaxSkips = 3;
	private int Player1Skips = MaxSkips;
	private int Player2Skips = MaxSkips;
	private int ActiveSkips => Turn == PlayerNum.Player1 ? Player1Skips : Player2Skips;
	private bool LastPlayerSkipped = false;
	private bool EnableSkips => SelectedStone == null && SelectedTile == null;
	private bool TurnEnded = false;
	public string Player1Name = "Player 1";
	public string Player2Name = "Player 2";
	public string ActivePlayerName => Turn == PlayerNum.Player1 ? Player1Name : Player2Name;
	public Text Player1Turn;
	public Text Player2Turn;
	public Text SkipsLeft;
	public Text Winner;
	public Text Options;
	public Button RevealPlayer1Stones;
	public Button RevealPlayer2Stones;
	public Button ActiveRevealButton => Turn == PlayerNum.Player1 ? RevealPlayer1Stones : RevealPlayer2Stones;
	public Button SkipButton;
	public Button AttackButton;

	public RemovedStoneController Player1RemovedStoneLocation;
	public RemovedStoneController Player2RemovedStoneLocation;

	public bool HideStones = false;

	private int MaxNumMoves = 1;
	private int NumActions = 1;

	private bool FirstAttack = true;

	public BoardTile SelectedTile {
		get => Board.SelectedTile;
		set => Board.SelectedTile = value;
	}
	public Stone SelectedStone {
		get => Board.SelectedStone;
		set => Board.SelectedStone = value;
	}

	private Stone LastSwapped;

	public Stone AlreadyMovedStone;


	private void Awake() {
		if (Game == null) {
			Game = this;
		}

		if (ScenePersistantInfo.Info?.HideStones == false) {
			RevealPlayer1Stones.gameObject.SetActive(false);
			RevealPlayer2Stones.gameObject.SetActive(false);
			HideStones = false;
		}
		else {
			RevealPlayer1Stones.gameObject.SetActive(true);
			RevealPlayer2Stones.gameObject.SetActive(true);
			HideStones = true;
		}
	}


	// Start is called before the first frame update
	public void Start() {
		Winner.gameObject.SetActive(false);
		SkipsLeft.gameObject.SetActive(false);
		
		
		Sounds = GetComponent<AudioSource>();
		Debug.Log(Board != null ? "The GameManager has successfully loaded the board" : "The GameManager has failed to laod the board");

		int j = Player1Stones.Count - 1;
		System.Random rnd = new System.Random((int)DateTime.Now.Ticks);

		Player1Name = ScenePersistantInfo.Info?.Player1Name;
		Player2Name = ScenePersistantInfo.Info?.Player2Name;
		Player1Turn.text = $"{Player1Name}'s turn";
		Player2Turn.text = $"{Player2Name}'s turn";


		//create the pairs
		int pairedID = 0;
		for (int i = 0; i < Player1Stones.Count; i = i + 2) {
			try {
				Player1Stones[i + 1].OtherHalf = Player1Stones[i];
				Player1Stones[i].OtherHalf = Player1Stones[i + 1];
				RemovedStonesPairsPlayer1[pairedID] = false;
				Player1Stones[i].PairedID = pairedID;
				Player1Stones[i + 1].PairedID = pairedID++;

				Player2Stones[i + 1].OtherHalf = Player2Stones[i];
				Player2Stones[i].OtherHalf = Player2Stones[i + 1];
				RemovedStonesPairsPlayer2[pairedID] = false;
				Player2Stones[i].PairedID = pairedID;
				Player2Stones[i + 1].PairedID = pairedID++;
			}
			catch (Exception ex) {
				Debug.LogError($"Error happened while pairing stones: {ex.Message}");
			}
		}

		if (ScenePersistantInfo.Info?.HideStones == true) {
			//shuffle them for a future feature where their identities will be hidden
			for (int i = 0; i < j; i++) {
				Swap(Player1Stones, i, rnd.Next(i, j));
				Swap(Player2Stones, i, rnd.Next(i, j));
			}

			foreach (var stone in Player1Stones) {
				stone.IsRevealed = false;
			}
			foreach (var stone in Player2Stones) {
				stone.IsRevealed = false;
			}
		}

		//set thier coordinates on the game board
		for (int i = j; i >= 0; i--) {
			var stone1Cell = Grid.WorldToCell(Player1Stones[i].transform.position);
			var stone2Cell = Grid.WorldToCell(Player2Stones[i].transform.position);
			Player1Stones[i].Coordinates = (stone1Cell.x, stone1Cell.y);
			Player2Stones[i].Coordinates = (stone2Cell.x, stone2Cell.y);
			if (HideStones == true) {
				Player1Stones[i].CurrentColor = Color.white;
				Player2Stones[i].CurrentColor = Color.white;
			}
		}

		//set well coordinates
		foreach (var well in LodenWells) {
			var cell = Grid.WorldToCell(well.transform.position);
			well.Coordinates = (cell.x, cell.y);
		}

		InstantlyResetWells();

	}

	private void InstantlyResetWells() {
		Board.InstantlyMoveWellToTile(LodenWells[0], Board.Tiles[1, 1]);
		Board.InstantlyMoveWellToTile(LodenWells[1], Board.Tiles[1, 4]);
		Board.InstantlyMoveWellToTile(LodenWells[2], Board.Tiles[4, 4]);
		Board.InstantlyMoveWellToTile(LodenWells[3], Board.Tiles[4, 1]);
		Player1Wells = new List<Well>();
		Player2Wells = new List<Well>();
	}

	private bool reseting = false;
	public IEnumerator ResetWells() {
		if (!reseting) {
			reseting = true;
			StartCoroutine(Board.MoveWellToTile(LodenWells[0], Board.Tiles[1, 1]));
			StartCoroutine(Board.MoveWellToTile(LodenWells[1], Board.Tiles[1, 4]));
			StartCoroutine(Board.MoveWellToTile(LodenWells[2], Board.Tiles[4, 4]));
			yield return StartCoroutine(Board.MoveWellToTile(LodenWells[3], Board.Tiles[4, 1]));
			Player1Wells = new List<Well>();
			Player2Wells = new List<Well>();
			reseting = false;
		}
	}

	public void Update() {
		if (Turn == PlayerNum.Player1) {
			Player1Turn.gameObject.SetActive(true);
			Player2Turn.gameObject.SetActive(false);
			
			RevealPlayer1Stones.gameObject.SetActive(HideStones == true);
			RevealPlayer2Stones.gameObject.SetActive(false);
		}
		else {
			Player1Turn.gameObject.SetActive(false);
			Player2Turn.gameObject.SetActive(true);
			
			RevealPlayer1Stones.gameObject.SetActive(false);
			RevealPlayer2Stones.gameObject.SetActive(HideStones == true);
		}

		//if (EnableAttacks || Debugging) {
		//	Board.AttackButton.SetActive(
		//		SelectedStoneController?.InPlay == true 
		//	 && SelectedStoneController.Player == Turn
		//	 && NumActions == MaxNumMoves
		//	);
		//}
		//else if (Board.AttackButton.activeSelf) {
		//	Board.AttackButton.SetActive(false);
		//}

		if (EnableSkips || Debugging) {
			SkipButton.interactable = true;
			if (LastPlayerSkipped) {
				SkipButton.GetComponentInChildren<Text>().text = "End Game";
			}
			else {
				SkipButton.GetComponentInChildren<Text>().text = "Pass Turn";
			}
		}
		else {
			SkipButton.interactable = false;
		}
	}

	private void Swap(List<Stone> stones, int i, int v) {
		var temp = stones[i].transform.position;
		var tempStone = stones[i];
		stones[i].transform.position = stones[v].transform.position;
		stones[i] = stones[v];
		stones[v].transform.position = temp;
		stones[v] = tempStone;
	}

	public void ResetGame() {
		ResetStones(Player1Stones);
		ResetStones(Player2Stones);
		RemovedStones = new List<Stone>();
		ResetWells(LodenWells);
		Board.ClearIndicators();
	}

	private void ResetStones(List<Stone> stones) {
		foreach (var stone in stones) {
			if (stone.InPlay) {
				Board.InstantlyMoveStoneToCellFromTile(stone, stone.StartingCoordinates, Board.Tiles[stone.X, stone.Y]);
			}
			else if (RemovedStones.Contains(stone)) {
				Board.InstantlyMoveGameTokenToCell(stone, stone.StartingCoordinates);
			}
			if (ScenePersistantInfo.Info?.HideStones == null) {
				stone.IsRevealed = false;
				stone.CurrentColor = Color.white;
			}
		}
	}

	private void ResetWells(List<Well> wells) {
		foreach (var well in wells) {
			if (well.InPlay) {
				Board.InstantlyMoveWellToCellFromTile(well, well.StartingCoordinates, Board.Tiles[well.X, well.Y]);
			}
			else {
				Board.InstantlyMoveGameTokenToCell(well, well.StartingCoordinates);
			}
		}
		InstantlyResetWells();
		Board.ResetQuadrants();
	}



	private IEnumerator ProgressTurn(bool playerSkipped = false) {
		TurnEnded = true;
		//yield return new WaitForSecondsRealtime(.5f);
		//while (Board.ObjectsAreMoving) {
			yield return null;
		//}
		StartCoroutine(Board.RemoveSelection());


		//NumActions -= cost;
		//if (NumActions <= 0) {
		Turn = Turn == PlayerNum.Player1 ? PlayerNum.Player2 : PlayerNum.Player1;

		if (playerSkipped && LastPlayerSkipped == true) {

			int player1Count = RemovedStones.Count(x => x.Player == PlayerNum.Player1);
			int player2Count = RemovedStones.Count(x => x.Player == PlayerNum.Player2);

			if (player1Count > player2Count) {
				ShowWinner(PlayerNum.Player1, $"{Player2Name} removed less stones");
			}
			else if (player2Count > player1Count) {
				ShowWinner(PlayerNum.Player2, $"{Player1Name} removed less stones");
			}
			else {
				ShowWinner(PlayerNum.Player2, $"There was a tie, and second player wins ties.");
			}

			
		}
		else {
			LastPlayerSkipped = playerSkipped;
		}

		Board.ResetQuadrants();

		LastSwapped = null;
		SkipsLeft.text = $"Skips Left: {ActiveSkips}";
		CheckForWinner();

		TurnEnded = false;
	}

	public void CheckForWinner() {
		bool Player1Loses = !RemovedStonesPairsPlayer1.Any(x => x.Value == false);
		bool Player2Loses = !RemovedStonesPairsPlayer2.Any(x => x.Value == false);

		//a tie.
		if (Player1Loses && Player2Loses) {
			//second player wins the second tie.
			ShowWinner(RemovedStones.Count(x => x.Player == PlayerNum.Player1) > RemovedStones.Count(x => x.Player == PlayerNum.Player2)
				? PlayerNum.Player1 : PlayerNum.Player2,
				"of a tie");
		}
		else if (Player1Loses) {
			ShowWinner(PlayerNum.Player2, $"{Player1Name} can no longer attack");
		}
		else if (Player2Loses) {
			ShowWinner(PlayerNum.Player1, $"{Player2Name} can no longer attack");
		}
	}

	private void EnableAttacksIfPossible() {
		//if (EnableAttacks == false &&
		//	(Player1Stones.Count(x => x.InPlay) >= 2 || Player2Stones.Count(x => x.InPlay) >= 2)) {
		EnableAttacks = true;
		//}
	}

	public void TileClicked(BoardTile tile) {
		if (TurnEnded) {
			StartCoroutine(PlayerErrorSound());
			return;
		}
		Board.ClearIndicators();
		Board.ResetQuadrants();

		//removing selection;
		if (SelectedTile?.Coordinates == tile.Coordinates) {
			StartCoroutine(Board.RemoveSelection());
			SelectedTile = null;
			return;
		}

		List<Well> currentWells = ActiveWells; // this prevents the turn changing from changing which list we get.
		;
		Debug.Log($"SSC: { SelectedStone != null} IsValid: {SelectedStone?.IsSelected} HasStone: {tile.HasStone} HasWell: {tile.HasWell}");

		bool isNearWell = Board.IsWellNear(tile);
		bool hasWell = currentWells.Count(x => x != null) > 0;

		//Will a stone be moved to the tile from reserve?
		if (SelectedStone != null
			&& SelectedStone.InPlay == false
			&& SelectedStone.IsSelected
			&& !tile.HasStone
			&& (isNearWell || hasWell)) {
			PlayMoveSound();
			//Will a well be moved to the tile?
			if (isNearWell == false && tile.HasWell == false && hasWell) {
				var well = currentWells.First(x => x != null).GetComponent<Well>();
				StartCoroutine(Board.MoveWellToTile(well, tile));
				currentWells.Remove(well);
				currentWells = currentWells.OrderBy(x => x != null).ToList();
				foreach (var remainingWell in currentWells) {
					if (remainingWell != null) {
						StartCoroutine(Board.MoveWellToCell(remainingWell, (remainingWell.X, remainingWell.Y + 1)));
					}
				}
				StartCoroutine(Board.MoveStoneToTile(SelectedStone, tile));
				Board.Selection.SetActive(false);
				//Might eventually shift all stones up here, haven't done that yet.
				//ShuffleStonesUpdward(blah blah blah);
				return;
			}
			else {
				var MovedStone = SelectedStone;
				StartCoroutine(Board.MoveStoneToTile(SelectedStone, tile));
				Board.Selection.SetActive(false);
				//Might eventually shift all stones up here, haven't done that yet.
				//ShuffleStonesUpdward(blah blah blah);
				StartCoroutine(ProgressTurn());
				return;
			}
		}
		//will a stone be moved from the board to somewhere else on the board?
		if (SelectedStone?.InPlay == true) {
			Quadrant quad = Board.GetQuadrant(SelectedStone.Coordinates);
			if (SelectedStone.InPlay == true
				&& SelectedStone.IsSelected
				&& !tile.HasStone
				&& Board.IsInQuadrant(quad, tile.Coordinates)
				&& Board.QuadrantHasWell(quad)) {
				StartCoroutine(Board.MoveStoneToTileFromTile(SelectedStone, tile, Board.Tiles[SelectedStone.X, SelectedStone.Y]));
				Board.Selection.SetActive(false);
				//Might eventually shift all stones up here, haven't done that yet.
				//ShuffleStonesUpdward(blah blah blah);
				StartCoroutine(ProgressTurn());
				PlayMoveSound();
				return;
			}
		}
			//selection is invalid
		if (SelectedTile != null) {
			SelectedTile.UnSelect();
		}
		if (SelectedStone != null) {
			SelectedStone.UnSelect();
			SelectedStone = null;
		}

		SelectedTile = tile;

		StartCoroutine(Board.ShowCellAsSelected(tile.Coordinates, Invalid));
		StartCoroutine(PlayerErrorSound());
	}

	public void StoneClicked(Stone stone) {
		Debug.Log($"Stone Coordinates: {stone.Coordinates}");
		if (TurnEnded) {
			StartCoroutine(PlayerErrorSound());
			return;
		}
		//unselect a stone
		if (SelectedStone?.Coordinates == stone.Coordinates) {
			StartCoroutine(Board.RemoveSelection());
			SelectedStone = null;
			return;
		}

		//if stone is already selected, are we swapping stones?
		if (SelectedStone?.Player == Turn  
			&& SelectedStone.InPlay == true 
			&& stone.InPlay
			&& stone.Coordinates != LastSwapped?.Coordinates
			&& Board.GetQuadrant(stone.Coordinates) == Board.GetQuadrant(SelectedStone.Coordinates)) {
			var lastSwapLocation = stone.Coordinates;
			StartCoroutine(Board.SwapStoneLocations(stone, SelectedStone));
			StartCoroutine(ProgressTurn());
			LastSwapped = Board.Tiles[lastSwapLocation.x, lastSwapLocation.y].Stone;
		
			return;
		}

		//select stone
		if (stone.Player == Turn) {
			if (stone.InPlay && !Board.QuadrantHasWell(Board.GetQuadrant(stone.Coordinates))) {
				StartCoroutine(Board.ShowCellAsSelected(stone.Coordinates, Invalid));
				SelectedStone?.UnSelect();
				SelectedStone = null;
				Board.ResetQuadrants();
				SelectedTile?.UnSelect();
				SelectedTile = null;
				StartCoroutine(PlayerErrorSound());
				return;
			}
			EnableAttacksIfPossible();
			Board.SelectQuadrant(stone);

			SelectedStone?.UnSelect();
			SelectedTile?.UnSelect();
			SelectedTile = null;
			SelectedStone = stone;
			Quadrant quad = Board.GetQuadrant(stone.Coordinates);
			SelectedStone.IsSelected = true;
			//enable or disable arrows according to stone's position on the board
			if (stone.InPlay && InActiveQuadrant(quad)) {
				var arrows = stone.Arrows.GetComponentsInChildren<Arrow>();
				foreach (var arrow in arrows) {
					int xCoor = arrow.Offset.x + stone.X;
					int yCoor = arrow.Offset.y + stone.Y;

					if (!IsValidTile(xCoor, yCoor)
						|| quad == Board.GetQuadrant((xCoor, yCoor))
						|| (xCoor, yCoor) == LastSwapped?.Coordinates) { //can't move here, hide arrow
						arrow.gameObject.GetComponent<BoxCollider2D>().enabled = false;
						arrow.Sprite.enabled = false;
					}
					else {
						arrow.gameObject.GetComponent<BoxCollider2D>().enabled = true;
						arrow.Sprite.enabled = true;

					}
				}
				stone.ArrowsShowing = true;

				//show indicators for potential move options
				if (stone.OtherHalf.InPlay == false 
					&& Board.IsWellNear(Board.Tiles[stone.X, stone.Y])
					&& stone.IsRevealed) {
					Board.IndicatePulseOption(stone);
					
				}
				else if (stone.OtherHalf.InPlay == true 
					&& IsValidAttack(stone.Coordinates, stone.OtherHalf.Coordinates)
					&& stone.IsRevealed) {

				}
			}
			else {
				Board.IndicateWellNear();
				stone.ArrowsShowing = false;
			}

			StartCoroutine(Board.ShowCellAsSelected(stone.Coordinates, !stone.InPlay || InActiveQuadrant(quad) ? Valid : Invalid));
			return;
		}
		else if (SelectedStone != null) {
			SelectedStone.UnSelect();
			SelectedStone = null;
			Board.ResetQuadrants();
		}

		if (SelectedTile != null) {
			SelectedTile.UnSelect();
			SelectedStone = null;
			SelectedTile = null;
		}

		StartCoroutine(Board.ShowCellAsSelected(stone.Coordinates, Invalid));
		//SelectedStoneController = stone;
		//SelectedStoneController.IsSelected = false;

		StartCoroutine(PlayerErrorSound());
		return;
	}

	private bool InActiveQuadrant(Quadrant quad) {
		return Board.WellsByQuadrant[quad] > 0;
	}

	internal void AttackButtonClicked() {
		if (TurnEnded) {
			StartCoroutine(PlayerErrorSound());
			return;
		}
		Board.ClearIndicators();
		if (SelectedStone == null || SelectedStone.InPlay == false) {
			StartCoroutine(PlayerErrorSound());
			return;
		}

		Stone sender = SelectedStone;
		Stone reciever = sender.OtherHalf;

		//I chose to prevent it this way instead of hiding the attack button because I didn't want the 
		//absense of an attack button give a clue to the threat level of an unrevealed stone.
		//if (RemovedStones.Contains(reciever)) {
		//	StartCoroutine(InvalidAttack());
		//	return;
		//}

		SelectedStone.ArrowsShowing = false;

		BoardTile senderTile = Board.Tiles[sender.X, sender.Y];
		
		//only one stone is on the board, this is valid and allows the retrieval of a well if the stone is on one
		//at the price of revealing the match for the stone used.
		if (!reciever.InPlay && Board.IsWellNear(senderTile)) {

			int xCell = 0;
			int yCell = 0;
			List<Well> wells = Board.GetWellsNear(sender.Coordinates);
			StartCoroutine(Board.SendStonePulse(sender, reciever));

			foreach (var well in wells) {
				if (ActiveWells.Count(x => x != null) < 2) {
					xCell = sender.Player == PlayerNum.Player1 ? GameBoard.Player1WellLocation : GameBoard.Player2WellLocation;
					yCell = GameBoard.LogicalBoardSize - ActiveWells.Count(x => x != null);

					ActiveWells = ActiveWells.OrderBy(x => x == null).ToList();
					ActiveWells.Insert(GameBoard.LogicalBoardSize - yCell, well);

					StartCoroutine(Board.MoveWellToStoneThenCellFromTile(well, reciever, (xCell, yCell), Board.Tiles[well.X, well.Y]));
				}
				else {
					xCell = sender.Player == PlayerNum.Player2 ? GameBoard.Player1WellLocation : GameBoard.Player2WellLocation;
					yCell = GameBoard.LogicalBoardSize - InActiveWells.Count(x => x != null);

					InActiveWells = InActiveWells.OrderBy(x => x == null).ToList();
					InActiveWells.Insert(GameBoard.LogicalBoardSize - yCell, well);

					StartCoroutine(Board.MoveWellToStoneThenCellFromTile(well, reciever, (xCell, yCell), Board.Tiles[well.X, well.Y]));
				}
			}
			PlayUnloadSound();
			StartCoroutine(Board.MoveStoneToCellFromTile(sender, sender.StartingCoordinates, senderTile));
		}
		else {
			BoardTile recieverTile = Board.Tiles[reciever.X, reciever.Y];
			var targets = Board.GetTargets(sender, reciever);
			if (reciever.InPlay && IsValidAttack(senderTile.Coordinates, reciever.Coordinates) && (targets.Count() > 0 || senderTile.HasWell || recieverTile.HasWell)) {
				StartCoroutine(Board.ValidOffensiveAttackSequence(sender, reciever, targets));
				PlayUnloadSound();
			}
			else {
				StartCoroutine(InvalidAttack());
				StartCoroutine(PlayerErrorSound());
				return;
			}
		}

		StartCoroutine(ProgressTurn());
	}

	internal void ArrowClicked(Stone stone, (int x, int y) direction) {
		var controller = stone;
		var startCoor = controller.Coordinates;
		(int x, int y) endCoor = (startCoor.x + direction.x, startCoor.y + direction.y);
		stone.ArrowsShowing = false;
		Board.Selection.SetActive(false);

		Stone otherStone = Board.Tiles[endCoor.x, endCoor.y].HasStone ? Board.Tiles[endCoor.x, endCoor.y].Stone : null;
		Board.ClearIndicators();

		//if stone is already selected, are we swapping stones?
		if (stone?.Player == Turn
			&& otherStone?.InPlay == true
			&& stone.InPlay) {
			if (otherStone.Coordinates != LastSwapped?.Coordinates) {
				Debug.Log($"Arrow swapping Coor: {stone.Coordinates} and Coor: {otherStone.Coordinates}");
				var lastSwapLocation = otherStone.Coordinates;
				StartCoroutine(Board.SwapStoneLocations(stone, otherStone));
				StartCoroutine(ProgressTurn());
				LastSwapped = Board.Tiles[lastSwapLocation.x, lastSwapLocation.y].Stone;
				return;
			}
			else {
				stone.ArrowsShowing = false;
				stone.UnSelect();
				StartCoroutine(Board.ShowCellAsSelected(otherStone.Coordinates, Invalid));
			}
		}
		else {
			StartCoroutine(Board.MoveStoneToTileFromTile(controller, Board.Tiles[endCoor.x, endCoor.y], Board.Tiles[startCoor.x, startCoor.y]));
			StartCoroutine(ProgressTurn());
		}
	}

	public bool IsValidAttack((int x, int y) startCoor, (int x, int y) endCoor) {
		if (startCoor.x != endCoor.x 
		 && startCoor.y != endCoor.y
		 && Math.Abs(startCoor.x - endCoor.x) != Math.Abs(startCoor.y - endCoor.y)) { 
			//not a valid stone use
			Debug.Log($"Attempted an invalid attack x: {startCoor.x - endCoor.x} != y: {startCoor.y - endCoor.y}");
			return false;
		}
		return true;
	}

	internal void SkipButtonClicked() {
		if (TurnEnded) {
			StartCoroutine(PlayerErrorSound());
			return;
		}
		Board.ClearIndicators();
		if (RemovedWells.Count > 0) {
			int xCell = Turn == PlayerNum.Player1 ? GameBoard.Player1WellLocation : GameBoard.Player2WellLocation;
			int yCell = GameBoard.LogicalBoardSize - ActiveWells.Count(x => x != null);

			Well well = RemovedWells[0];
			RemovedWells.Remove(well);

			ActiveWells = ActiveWells.OrderBy(x => x == null).ToList();
			ActiveWells.Insert(GameBoard.LogicalBoardSize - yCell, well);

			StartCoroutine(Board.MoveWellToCell(well, (xCell, yCell)));
		}

		StartCoroutine(ProgressTurn(true));

		LastPlayerSkipped = true;
	}

	public void RemovedFromGame(Stone stone) {
		if (stone.Player == PlayerNum.Player1) {
			RemovedStonesPairsPlayer1[stone.PairedID] = true;
		}
		else {
			RemovedStonesPairsPlayer2[stone.PairedID] = true;
		}
		stone.GetComponent<BoxCollider2D>().enabled = false;
		RemovedStones.Add(stone);
		if (ScenePersistantInfo.Info?.HideStones == true) {
			StartCoroutine(Board.FadeStone(stone, stone.OriginalColor, Color.white, .01f, false));
			stone.IsRevealed = true;
		}
	}



	private bool IsValidTile(int x, int y) {
		return GameBoard.OnGameBoard((x, y));
	}

	private void ShowWinner(PlayerNum player, string reason) {
		string winner = player == PlayerNum.Player1 ? Player1Name : Player2Name;
		SkipButton.gameObject.SetActive(false);
		AttackButton.gameObject.SetActive(false);
		Winner.text = $"{winner} won because {reason}";
		Winner.gameObject.transform.parent.gameObject.SetActive(true);
		Winner.gameObject.SetActive(true);

		Turn = PlayerNum.GameOver;
	}



	private Grid Grid {
		get {
			return Board.Board;
		}
	}

	internal IEnumerator InvalidAttack() {
		AttackButton.GetComponent<Image>().color = Color.red;
		yield return new WaitForSecondsRealtime(5);
		AttackButton.GetComponent<Image>().color = Color.white;
		yield return Board.RemoveSelection();
	}

	public void RevealLodenStones() {
		StartCoroutine(Board.RevealLodenStone(ActiveStones));
	}

	public void ShowMenu() {
		if (Winner.gameObject.transform.parent.gameObject.activeSelf) {
			Winner.gameObject.transform.parent.gameObject.SetActive(false);
			Options.text = "Options";
		}
		else {
			Winner.gameObject.transform.parent.gameObject.SetActive(true);
			Options.text = "Back";
		}
	}

	public AudioClip GetErrorSound() {
		var rand = new System.Random();
		int num = rand.Next(0, 100);
		if (num <= 70) {
			return Error;
		}
		else if (num <= 90) {
			return CriticalError;
		}
		else {
			return CantTouchThis;
		}
	}

	public IEnumerator PlayerErrorSound() {
		var clip = Sounds.clip = GetErrorSound();
		if (clip.loadState == AudioDataLoadState.Loaded) {
			var backgroundMusic = ScenePersistantInfo.Info.GetComponent<AudioSource>();
			if (backgroundMusic != null) {
				backgroundMusic.mute = true;
				Sounds.Play();
				yield return Sounds.isPlaying;
				backgroundMusic.mute = false;
			}
		}
	}

	public void PlayMoveSound() {
		Sounds.clip = Pulse;
		Sounds.Play();
	}

	public void PlayUnloadSound() {
		Sounds.clip = Stream;
		Sounds.Play();
	}

}
