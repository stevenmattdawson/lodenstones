﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
	//These have to match the buildIndex given in Unity's Build settings.
	public const int StartMenu = 0;
	public const int Tutorial = 1;
	public const int Game = 2;
	public const int FuturePlans = 3;

	public static void SwitchToScene(int buildIndex) {
		SceneManager.LoadScene(buildIndex);
	}

	public void SwitchToTutorial() {
		SceneManager.LoadScene(Tutorial);
	}
	public void SwitchToGame() {
		SceneManager.LoadScene(Game);
	}
	public void SwitchToStartMenu() {
		SceneManager.LoadScene(StartMenu);
	}
	public void SwitchToFuturePlans() {
		SceneManager.LoadScene(FuturePlans);
	}
}

