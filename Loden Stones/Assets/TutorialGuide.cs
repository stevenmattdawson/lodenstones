﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialGuide : MonoBehaviour
{
	public List<Transform> Instructions;
	int index = 0;
	int next => index + 1;
	int previous => index - 1;

	Vector3 Position {
		get => transform.position;
		set => transform.position = new Vector3(value.x, value.y, -16.4f);
	}
    // Start is called before the first frame update
    void Start()
    {
		Position = Instructions[index].transform.position;
    }

	public void MoveToNextInstruction() {
		if (next < Instructions.Count && Instructions[next] != null) {
			StartCoroutine(MoveCameraToPosition(Instructions[++index].position));
		}
	}

	public void MoveToPreviousInstruction() {
		if (previous >= 0 && Instructions[previous] != null) {
			StartCoroutine(MoveCameraToPosition(Instructions[--index].position));
		}

	}

	public void ReturnToMainMenu() {
		SceneManager.LoadScene(1);
	}

	public IEnumerator MoveCameraToPosition(Vector3 newPosition, int speed = 50) {
		var xDistance = newPosition.x - transform.position.x;
		var yDistance = newPosition.y - transform.position.y;
		var xStep = xDistance / speed;
		var yStep = yDistance / speed;

		for (int i = 0; i < speed; i++) {
			transform.Translate(new Vector3(xStep, yStep));
			yield return new WaitForSecondsRealtime(.01f);
		}
		Position = newPosition;
	}
}
