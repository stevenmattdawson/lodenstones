﻿


using UnityEngine;

namespace Assets.GameTokens {
	public class Well : GameToken {

		public Color InPulseRange;
		public Color Normal;

		public override string ToString() {
			return $"Well Coor - {base.ToString()}";
		}
	}
}
