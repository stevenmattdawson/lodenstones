﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Assets.GameTokens {
	public class Arrow : GameToken {
		public enum ArrowDirection {
			Up,
			Down,
			Left,
			Right,
			UpRight,
			UpLeft,
			DownRight,
			DownLeft,
		}

		public ArrowDirection Direction;
		Stone Stone;
		private SpriteRenderer _sprite;
		public SpriteRenderer Sprite {
			get {
				if (_sprite == null) {
					_sprite = GetComponentInChildren<SpriteRenderer>();
				}
				return _sprite;
			}
		}
		// Start is called before the first frame update
		void Start() {
			Stone = this.transform.parent.parent.gameObject.GetComponent<Stone>();
			
			Debug.Log($"Arrows found {Stone.name} as thier parent");
		}

		// Update is called once per frame
		void Update() {

		}

		private void OnMouseDown() {
			GameManager.Game.ArrowClicked(Stone, Offset);
			Stone.ArrowsShowing = false;
		}

		public (int x, int y) Offset {
			get {
				switch (Direction) {
					case ArrowDirection.Up:
						return (0, 1);
					case ArrowDirection.Down:
						return (0, -1);
					case ArrowDirection.Left:
						return (-1, 0);
					case ArrowDirection.Right:
						return (1, 0);
					case ArrowDirection.UpRight:
						return (1, 1);
					case ArrowDirection.UpLeft:
						return (-1, 1);
					case ArrowDirection.DownRight:
						return (1, -1);
					case ArrowDirection.DownLeft:
						return (-1, -1);
					default:
						Debug.Assert(false, "Arrow's Directional Offset was null");
						return (0, 0);
				}
			}
		}
	}
}