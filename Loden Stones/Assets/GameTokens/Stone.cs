﻿
using UnityEngine;


namespace Assets.GameTokens {
	public enum PlayerNum {
		Player1,
		Player2,
		GameOver
	}

	public class Stone : GameToken {
		public GameObject Arrows;
		public SpriteRenderer ColorIndicator;
		private SpriteRenderer ActiveIndicator;
		public readonly Color Disabled = Color.gray;
		public readonly Color Active = Color.black;

		private void Awake() {
			OriginalColor = ColorIndicator.color;
			ActiveIndicator = GetComponent<SpriteRenderer>();
		}
		// Start is called before the first frame update
		void Start() {
			
		}

		// Update is called once per frame
		void Update() {
			if (Arrows.activeSelf != ArrowsShowing) {
				Arrows.SetActive(ArrowsShowing);
			}
			if (ActiveIndicator.color != Active && !InPlay) {
				ActiveIndicator.color = Active;
			}
		}


		private void OnMouseDown() {
			GameManager.Game.StoneClicked(this);
		}

		public void UnSelect() {
			IsSelected = false;
			ArrowsShowing = false;
			AttackButtonShowing = false;
		}



		public void ShowDisabled() {
			ActiveIndicator.color = Disabled;
		}

		public void ShowActive() {
			ActiveIndicator.color = Active;
		}

		public bool ShownAsActive => ActiveIndicator.color == Active;

		#region Data



		public PlayerNum Player;
		public int PairedID;
		public Color OriginalColor;
		public bool _isRevealed = true;
		public bool IsRevealed {
			get {
				return _isRevealed;
			}
			set {
				_isRevealed = value;
			}
		}

		public Color CurrentColor {
			get => ColorIndicator.color;
			set {
				ColorIndicator.color = value;
			}
		}

		public bool _isSelected;
		public override bool IsSelected {
			get => _isSelected;
			set {
				if (value == false) {
					ArrowsShowing = false;
					AttackButtonShowing = false;
				}
				_isSelected = value;
			}
		}

		public Stone OtherHalf;


		public bool ArrowsShowing { get; internal set; }
		public bool AttackButtonShowing { get; internal set; }
		#endregion


		public override string ToString() {
			return $"Stone Coor - {base.ToString()}";
		}
	}
}
