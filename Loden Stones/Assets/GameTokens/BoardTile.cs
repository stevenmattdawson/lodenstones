﻿using System;
using UnityEngine;

namespace Assets.GameTokens {
	public class BoardTile : GameToken
	{

		public GameObject HasStuffIndicator;
		public SpriteRenderer ColorIndicator;

		public Color OnlyWell = Color.blue;
		public Color OnlyStone = Color.yellow;
		public Color Both = Color.green;

		public static Color HiddenColor = new Color(1, 1, 1, .4f);
		public static Color InvalidColor = new Color(1, .6947f, .6556f, .6f);
		public static Color ValidColor = new Color(.6582f, 1, .6549f, .6f);

		private void OnMouseUp() {
			GameManager.Game.TileClicked(this);
		}


		public void UnSelect() {
			IsSelected = false;
		}

		public void HideIndicatorColor() {
			ColorIndicator.color = HiddenColor;
			if (HasWell) {
				Well.GetComponent<SpriteRenderer>().color = Well.Normal;
			}
		}

		public void IndicateColor(bool isValid, bool hide = false) {
			ColorIndicator.color = hide ? HiddenColor : (isValid ? ValidColor : InvalidColor);
		}

		public void IndicateWellColor() {
			if (HasWell) {
				if (ColorIndicator.color == HiddenColor) {
					Well.GetComponent<SpriteRenderer>().color = Well.Normal;
				}
				else {
					Well.GetComponent<SpriteRenderer>().color = Well.InPulseRange;
				}
			}
		}


		// Update is called once per frame
		void Update()
		{
			try {
				
				HasStuffIndicator.SetActive(true);
				if (HasStone && HasWell) {
					HasStuffIndicator.GetComponent<SpriteRenderer>().color = Both;
				}
				else if (HasWell) {
					HasStuffIndicator.GetComponent<SpriteRenderer>().color = OnlyWell;

				}
				else if (HasStone) {
					HasStuffIndicator.GetComponent<SpriteRenderer>().color = OnlyStone;
				}
				else {
					HasStuffIndicator.SetActive(false);
				}
				
			}catch(Exception) {
				Debug.Log($"Exception Happened and Game was {GameManager.Game != null} and HasStoneIndicator was {HasStuffIndicator != null}");
			}
		}

		#region Data 
		public bool HasWell = false;
		public void PlaceWell(Well well) {

			HasWell = true;

			Well = well;
			Debug.Log($"Well has been placed at {Coordinates}");
		}

		public void RemoveWell() {
			HasWell = false;
			Debug.Log($"Well has been removed at {Coordinates}");
		}

		public bool HasStone = false;
		public void PlaceStone(Stone stone) {
			if (stone == null) {
				HasStone = false;
			}
			else {
				HasStone = true;
			}
			Stone = stone;
		}

		public void RemoveStone() {
			Stone = null;
			HasStone = false;

		}

		public Stone Stone;
		private Well _well;
		public Well Well {
			get {
				if (HasWell == false) {
					Debug.Log("Tried to access a null well");
					return null;
				}
				return _well;
			}
			set {
				_well = value;
				HasWell = true;
			}
		}
		#endregion

		public override string ToString() {
			return $"Tile Coor - {base.ToString()}";
		}
	}


}
