﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// A gametoken is defined as any logical unit of substance in the game. 
/// </summary>
namespace Assets.GameTokens {
	public class GameToken : MonoBehaviour {
		public int X {
			get => Coordinates.x;
			set => _coordinates = (value, _coordinates.y);

		}
		public int Y {
			get => Coordinates.y;
			set => _coordinates = (_coordinates.x, value);
		}

		bool initialized = false;
		private (int x, int y) _coordinates;
		public (int x, int y) Coordinates {
			get => _coordinates;
			set {
				if (initialized == false) {
					StartingCoordinates = value;
					initialized = true;
				}
				_coordinates = value;
			}
		}


		public virtual bool IsSelected {
			get;
			set;
		}

		public override string ToString() {
			return $"(X: {X} Y: {Y})";
		}

		public bool InPlay {
			get {
				return GameBoard.OnGameBoard(Coordinates);
			}
		}

		//used primarily by the stones, but used to save the starting locations of any game piece
		public (int x, int y) StartingCoordinates;
	}
}
