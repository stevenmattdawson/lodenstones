﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemovedStoneController : MonoBehaviour
{
	Transform[] locations;
	int i = 0;
    // Start is called before the first frame update
    void Start()
    {
		locations = GetComponentsInChildren<Transform>();
		Debug.Log($"{this.name} found {locations.Length} transforms for stone removal");
    }

	public Vector3 GetNextRemovedStoneLocation() {
		return locations[i++].position;
	}
}
