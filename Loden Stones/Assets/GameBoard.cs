﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.GameTokens;
using System.Linq;
using UnityEngine.UI;

namespace Assets {

	public enum Quadrant {
		LowerLeft,
		UpperLeft,
		UpperRight,
		LowerRight,
		None
	}

	public class GameBoard : MonoBehaviour {

		private bool Started = false;
		public Grid _grid;
		public Grid Board {
			get {
				if (_grid == null) {
					Start();
					Started = true;
				}
				return _grid;
			}
		}

		public GameManager Game => GameManager.Game;

		public const int BoardSize = 6; // must be an even number or quadrants won't be able to split correctly
		public const int LogicalBoardSize = BoardSize - 1;
		public const int QuadrantSize = BoardSize / 2;
		public const int Player1StoneLocation = -1;
		public const int Player2StoneLocation = BoardSize;
		public const int Player1WellLocation = Player1StoneLocation - 1;
		public const int Player2WellLocation = Player2StoneLocation + 1;

		private int numMovingObjects = 0;
		public bool ObjectsAreMoving => numMovingObjects == 0;

		float Fastest = .01f;
		float Fast = .05f;
		float Normal = .1f;
		float Slow = 1f;

		public BoardTile[,] Tiles = new BoardTile[BoardSize, BoardSize];

		public GameObject WindStream;
		public GameObject WindPulse;
		public GameObject Selection;

		public BoardTile SelectedTile;
		public Stone SelectedStone;

		public GameObject UpperRight;
		public GameObject UpperLeft;
		public GameObject LowerLeft;
		public GameObject LowerRight;

		internal Dictionary<Quadrant, int> WellsByQuadrant;

		//for Unit Testing
		public void Construct() {

		}

		public static GameObject ConstructLodenboard() {
			var lodenboard = new GameObject();
			lodenboard.AddComponent<Grid>();
			var board = lodenboard.AddComponent<GameBoard>();
			board.Construct();
			return lodenboard;
		}


		// Start is called before the first frame update
		public void Start() {
			if (Started == false) {
			
				_grid = GetComponent<Grid>();
				
				var components = GetComponentsInChildren<BoardTile>();

				foreach (var component in components) {
					//if (component.name == "LodenBoard") {
					//	continue;
					//}
					if (component.name.Contains("TileController")) {
						var cellCoor = Board.WorldToCell(component.gameObject.transform.position);
						component.GetComponent<BoardTile>().Coordinates = (cellCoor.x, cellCoor.y);
						Tiles[cellCoor.x, cellCoor.y] = component.GetComponent<BoardTile>();
					}
				}

				WellsByQuadrant = new Dictionary<Quadrant, int>();
				WellsByQuadrant[Quadrant.LowerLeft] = 0;
				WellsByQuadrant[Quadrant.LowerRight] = 0;
				WellsByQuadrant[Quadrant.UpperLeft] = 0;
				WellsByQuadrant[Quadrant.UpperRight] = 0;
			}

		//	StartCoroutine(AlwaysCheckWellState());
		}

		private int GetStreamStep(int sender, int reciever, int step) {
			if (sender > reciever) {
				step = -1;
			}
			else if (sender < reciever) {
				step = 1;
			}

			return step;
		}

		#region TileChecking 
		public static bool OnGameBoard((int x, int y) cell) {
			return cell.x >= 0 && cell.x <= LogicalBoardSize
				&& cell.y >= 0 && cell.y <= LogicalBoardSize;
		}

		public bool IsWellNear(BoardTile tile) {
			return GetWellsNear(tile.Coordinates).Count > 0;
		}

		public List<Well> GetWellsNear((int x, int y) cell) {
			int startX = cell.x - 1;
			int endX = cell.x + 1;
			int startY = cell.y - 1;
			int endY = cell.y + 1;
			List<Well> wells = new List<Well>();
			for (int x = startX; x <= endX; x++) {
				for (int y = startY; y <= endY; y++) {
					if (x >= 0 && x <= LogicalBoardSize && y >= 0 && y <= LogicalBoardSize) {
						if (Tiles[x, y].HasWell) {
							wells.Add(Tiles[x, y].Well);
						}
					}
				}
			}
			return wells;
		}


		//Horribly inefficient, but passable because the grid is guarenteed 6x6,
		//want to rework a more efficient algorith just for pride.
		public void IndicateWellNear() {
			bool canPlaceWell = GameManager.Game.ActiveWells.Count() > 0;
			foreach (var tile in Tiles) {
				tile.IndicateColor(canPlaceWell, IsWellNear(tile));

			}
		}

		public void IndicatePulseOption(Stone stone) {
			var wellsNear = GetWellsNear(stone.Coordinates);
			foreach (var well in wellsNear) {
				Tiles[well.X, well.Y].IndicateColor(true, stone.OtherHalf.InPlay);
				Tiles[well.X, well.Y].IndicateWellColor();
			}
		}

		public void ClearIndicators() {
			foreach (var tile in Tiles) {
				tile.HideIndicatorColor();
			}
		}


		public void ActivateStones(List<Stone> stones) {
			foreach (var stone in stones) {
				stone.ShowActive();
			}
		}

		public void DisableStones(List<Stone> stones) {
			foreach (var stone in stones) {
				stone.ShowDisabled();
			}
		}
		#endregion

		#region Quadrants
		public Quadrant GetQuadrant((int x, int y) cell) {
			if (OnGameBoard(cell)) {
				if (cell.x < QuadrantSize) {
					if (cell.y < QuadrantSize) {
						return Quadrant.LowerLeft;
					}
					else {
						return Quadrant.UpperLeft;
					}
				}
				else {
					if (cell.y < QuadrantSize) {
						return Quadrant.LowerRight;
					}
					else {
						return Quadrant.UpperRight;
					}
				}
			}
			return Quadrant.None;
		}

		public GameObject GetQuadrant(Quadrant quad) {
			switch (quad) {
				case Quadrant.LowerLeft:
					return LowerLeft;
				case Quadrant.UpperLeft:
					return UpperLeft;
				case Quadrant.UpperRight:
					return UpperRight;
				case Quadrant.LowerRight:
					return LowerRight;
				default:
					return null; // error case
			}
		}

		public Quadrant GetQuadrant(GameToken Token) {
			return GetQuadrant(Token.Coordinates);
		}

		public bool IsInQuadrant(Quadrant quad, (int x, int y) cell) {
			return quad == GetQuadrant(cell);
		}

		public bool CellIsNearQuadrant_Deprecated(Quadrant quad, (int x, int y) cell) {
			//function not used atm, I don't think it works either.
			if (cell.x == QuadrantSize) {
				if (cell.y < QuadrantSize && quad == Quadrant.LowerLeft) {
					return true;
				}
				else if (quad == Quadrant.UpperLeft) {
					return true;
				}
			}
			if (cell.x == QuadrantSize - 1) {
				if (cell.y < QuadrantSize && quad == Quadrant.LowerRight) {
					return true;
				}
				else if (quad == Quadrant.UpperRight) {
					return true;
				}
			}
			if (cell.y == QuadrantSize) {
				if (cell.x < QuadrantSize && quad == Quadrant.LowerLeft) {
					return true;
				}
				else if (quad == Quadrant.LowerRight) {
					return true;
				}
			}
			if (cell.y == QuadrantSize - 1) {
				if (cell.x < QuadrantSize && quad == Quadrant.UpperLeft) {
					return true;
				}
				else if (quad == Quadrant.UpperRight) {
					return true;
				}
			}
			return false;
		}

		public (int x, int y) GetDirectionVector_Deprecated(Quadrant nearQuad, Quadrant currentQuad) {
			switch (nearQuad) {
				case Quadrant.LowerLeft:
					if (currentQuad == Quadrant.UpperLeft) {
						return (0, -1);
					}
					else if (currentQuad == Quadrant.LowerRight) {
						return (-1, 0);
					}
					break;
				case Quadrant.UpperLeft:
					if (currentQuad == Quadrant.LowerLeft) {
						return (0, 1);
					}
					else if (currentQuad == Quadrant.UpperRight) {
						return (-1, 0);
					}
					break;
				case Quadrant.UpperRight:
					if (currentQuad == Quadrant.UpperLeft) {
						return (1, 0);
					}
					else if (currentQuad == Quadrant.LowerRight) {
						return (0, 1);
					}
					break;
				case Quadrant.LowerRight:
					if (currentQuad == Quadrant.UpperRight) {
						return (0, -1);
					}
					else if (currentQuad == Quadrant.LowerLeft) {
						return (1, 0);
					}
					break;
				case Quadrant.None:
					Debug.LogError($"Directional Vector was called for a near: {nearQuad} and current: {currentQuad} that were invalid.");
					return (0, 0);
			}
			return (0, 0);
		}

		public void ActivateQuadrant(bool value, Quadrant quad) {
			switch (quad) {
				case Quadrant.LowerLeft:
					LowerLeft.SetActive(value);
					break;
				case Quadrant.UpperLeft:
					UpperLeft.SetActive(value);
					break;
				case Quadrant.UpperRight:
					UpperRight.SetActive(value);
					break;
				case Quadrant.LowerRight:
					LowerRight.SetActive(value);
					break;
				case Quadrant.None:
					break;
				default:
					break;
			}
		}

		public void SelectQuadrant(Stone stone) {
			if (stone?.InPlay == true) {
				Quadrant stoneQuad = GetQuadrant(stone);
				foreach (var quad in Enum.GetValues(typeof(Quadrant)).Cast<Quadrant>()) {
					ActivateQuadrant(quad == stoneQuad, quad);
				}
			}
		}

		public void ResetQuadrants() {
			foreach (var quad in Enum.GetValues(typeof(Quadrant)).Cast<Quadrant>()) {
				if (QuadrantHasWell(quad)){
					ActivateQuadrant(true, quad);
				}
				else {
					ActivateQuadrant(false, quad);
				}
			}
		}

		public bool QuadrantHasWell(Quadrant quad) {
			if (quad != Quadrant.None) {
				return WellsByQuadrant[quad] > 0;
			}
			return false;
		}

		public bool QuadrantHasWell((int x, int y) cell) {
			return QuadrantHasWell(GetQuadrant(cell));
		}

		public void AddWellToQuadrant(Quadrant quad) {
			WellsByQuadrant[quad]++;
		}

		public void RemoveWellFromQuadrant(Quadrant quad) {
			WellsByQuadrant[quad]--;
			if (WellsByQuadrant[quad] < 0) {
				WellsByQuadrant[quad] = 0;
			}
		}

		public int NumWellsInPlay => WellsByQuadrant.Values.Sum();
		public bool NoWellInPlay => NumWellsInPlay == 0;

		public List<Stone> GetStonesInQuadrant(Quadrant quad) {
			var tiles = GetTilesInQuadrant(quad);
			List<Stone> stones = new List<Stone>();
			foreach (var tile in tiles) {
				if (tile.HasStone) {
					stones.Add(tile.Stone);
				}
			}
			return stones;
		}

		public List<Stone> GetStonesInQuadrant((int x, int y) cell) {
			return GetStonesInQuadrant(GetQuadrant(cell));
		}

		public List<BoardTile> GetTilesInQuadrant(Quadrant quad) {
			List<BoardTile> tiles = new List<BoardTile>();
			int xLowerBoundary = 0;
			int yLowerBoundary = 0;
			int xUpperBoundary = 0;
			int yUpperBoundary = 0;
			switch (quad) {
				case Quadrant.LowerLeft:
					xLowerBoundary = 0;
					yLowerBoundary = 0;
					xUpperBoundary = 3;
					yUpperBoundary = 3;
					break;
				case Quadrant.UpperLeft:
					xLowerBoundary = 0;
					yLowerBoundary = 3;
					xUpperBoundary = 3;
					yUpperBoundary = 6;
					break;
				case Quadrant.UpperRight:
					xLowerBoundary = 3;
					yLowerBoundary = 3;
					xUpperBoundary = 6;
					yUpperBoundary = 6;
					break;
				case Quadrant.LowerRight:
					xLowerBoundary = 3;
					yLowerBoundary = 0;
					xUpperBoundary = 6;
					yUpperBoundary = 3;
					break;
				case Quadrant.None:
					break;
			}
			for (int x = xLowerBoundary; x < xUpperBoundary; x++) {
				for (int y = yLowerBoundary; y < yUpperBoundary; y++) {
					tiles.Add(Tiles[x, y]);
				}
			}
			return tiles;
		}

		public List<BoardTile> GetTilesInQuadrant((int x, int y) cell) {
			return GetTilesInQuadrant(GetQuadrant(cell));
		}
		#endregion

		#region UpdateState Logic

		private void UnselectStone(Stone stone, (int x, int y) cell) {
			stone.UnSelect();
			stone.Coordinates = cell;
			if (SelectedStone == stone) {
				SelectedStone = null;
			}
		}

		private void EnableActiveStone(Stone stone, (int x, int y) cell) {
			if (QuadrantHasWell(GetQuadrant(cell))) {
				stone.ShowActive();
			}
			else {
				stone.ShowDisabled();
			}
		}

		private void PlaceStoneOnTile(Stone stone, BoardTile tile) {
			tile.UnSelect();
			tile.PlaceStone(stone);
			SelectedTile = null;
		}

		private void PlaceWellOnTile(Well well, BoardTile tile) {
			tile.PlaceWell(well);
			Quadrant quad = GetQuadrant(tile.Coordinates);
			AddWellToQuadrant(quad);
			ActivateQuadrant(true, quad);
			ActivateStones(GetStonesInQuadrant(quad));
		}

		private void RemoveWellFromTile(Well well, BoardTile start) {
			start.UnSelect();
			start.RemoveWell();
			Quadrant quad = GetQuadrant(well.Coordinates);
			RemoveWellFromQuadrant(quad);
			var stones = GetStonesInQuadrant(quad);
			if (WellsByQuadrant[quad] > 0) {
				ActivateQuadrant(true, quad);
				ActivateStones(stones);
			}
			else {
				ActivateQuadrant(false, quad);
				DisableStones(stones);
			}
		}
		#endregion

		#region Enumeration
		public IEnumerator ValidOffensiveAttackSequence(Stone sender, Stone reciever, List<(int x, int y)> targets) {
			yield return SendStoneStream(sender, reciever, targets);
			foreach (var target in targets) {
					
				Debug.Log($"Found Target x:{target.x} y: {target.y}");
				ClearTarget(target.x, target.y);
					
			}
			BoardTile senderTile = Tiles[sender.X, sender.Y];
			BoardTile recieverTile = Tiles[reciever.X, reciever.Y];

			//Moves wells to opposite stone
			if (senderTile.HasWell == true && recieverTile.HasWell == true) {
				StartCoroutine(MoveWellToTileFromTile(senderTile.Well, recieverTile, senderTile));
				StartCoroutine(MoveWellToTileFromTile(recieverTile.Well, senderTile, recieverTile));
				recieverTile.HasWell = true;
				senderTile.HasWell = true;
			}
			else if (senderTile.HasWell == true) {
				StartCoroutine(MoveWellToTileFromTile(senderTile.Well, recieverTile, senderTile));
			}
			else if (recieverTile.HasWell == true) {
				StartCoroutine(MoveWellToTileFromTile(recieverTile.Well, senderTile, recieverTile));
			}

			StartCoroutine(MoveStoneToCellFromTile(sender, sender.StartingCoordinates, senderTile));
		//	StartCoroutine(MoveStoneToCellFromTile(reciever, reciever.StartingCoordinates, recieverTile));
			GameManager.Game.CheckForWinner();
		}

		public List<(int x, int y)> GetTargets(Stone sender, Stone reciever) {
			int xSender = sender.Coordinates.x;
			int ySender = sender.Coordinates.y;
			int xReciever = reciever.Coordinates.x;
			int yReciever = reciever.Coordinates.y;

			List<(int x, int y)> targets = new List<(int x, int y)>();
			//gets the direction we will be traversing;
			int xStep = 0;
			int yStep = 0;
			xStep = GetStreamStep(xSender, xReciever, xStep);
			yStep = GetStreamStep(ySender, yReciever, yStep);
			targets = new List<(int x, int y)>();
			Debug.Log($"XStep:{xStep} YStep:{yStep} --- X:{xSender + xStep} Y:{ySender + yStep} XSender:{xSender} YSender:{ySender} --- XReciever:{xReciever} YReciever:{yReciever}");
			int x = xSender + xStep;
			int y = ySender + yStep;

			if (x == xReciever && y != yReciever) {
				for (; y != yReciever; y += yStep) {
					Debug.Log($"XStep:{xStep} YStep:{yStep} --- X:{x} Y:{y} XSender:{xSender} YSender:{ySender} --- XReciever:{xReciever} YReciever:{yReciever}");
					Debug.Log($"Found X: {x == xReciever} Found Y: {y == yReciever}");
					if (Tiles[x, y].HasStone) {
						targets.Add((x, y));
					}
				}
			}
			else if (x != xReciever && y == yReciever) {
				for (; x != xReciever; x += xStep) {
					Debug.Log($"XStep:{xStep} YStep:{yStep} --- X:{x} Y:{y} XSender:{xSender} YSender:{ySender} --- XReciever:{xReciever} YReciever:{yReciever}");
					Debug.Log($"Found X: {x == xReciever} Found Y: {y == yReciever}");
					if (Tiles[x, y].HasStone) {
						targets.Add((x, y));
					}
				}
			}
			else if (x != xReciever && y != yReciever) {
				for (; (x, y) != (xReciever, yReciever);) {
					Debug.Log($"XStep:{xStep} YStep:{yStep} --- X:{x} Y:{y} XSender:{xSender} YSender:{ySender} --- XReciever:{xReciever} YReciever:{yReciever}");
					if (Tiles[x, y].HasStone) {
						Debug.Log($"Found X: {x == xReciever} Found Y: {y == yReciever}");
						targets.Add((x, y));
					}
					x += xStep;
					y += yStep;
				}
			}
			return targets;
		}

		public IEnumerator ValidDeffensiveAttackSequence(Stone sender, Stone reciever, BoardTile tile, Well well, (int x, int y) cell) {
			new WaitForSeconds(Normal);
			if (well != null) {
				yield return MoveWellToStoneThenCellFromTile(well, reciever, cell, tile);
			}
			
		}

		private void ClearTarget(int x, int y) {
			if (Tiles[x, y].HasStone) {
				GameManager.Game.RemovedFromGame(Tiles[x, y].Stone);
				Vector3Int RemovedLocation;
				if (Tiles[x,y].Stone.Player == PlayerNum.Player1) {
					RemovedLocation = Board.WorldToCell(GameManager.Game.Player2RemovedStoneLocation.GetNextRemovedStoneLocation());
				}
				else {
					RemovedLocation = Board.WorldToCell(GameManager.Game.Player1RemovedStoneLocation.GetNextRemovedStoneLocation());
				}
				StartCoroutine(MoveStoneToCellFromTile(Tiles[x, y].Stone, (RemovedLocation.x, RemovedLocation.y), Tiles[x, y]));
			}
		}

		public IEnumerator SendStoneStream(Stone sender, Stone reciever, List<(int x, int y)> targets) {
			var senderPulseClone = Instantiate(WindPulse, sender.transform.position, new Quaternion(0, 0, 0, 1));
			List<GameObject> toBeDestroyed = new List<GameObject>();
			foreach (var target in targets) {
				toBeDestroyed.Add(Instantiate(WindPulse, Tiles[target.x, target.y].Stone.transform.position, new Quaternion(0, 0, 0, 1)));
			}
			GameObject recieverPulseClone = new GameObject();
			float fadeSpeed = .05f;
			StartCoroutine(FadeStone(sender, sender.OriginalColor, Color.white, fadeSpeed, false));
			StartCoroutine(FadeStone(reciever, reciever.OriginalColor, Color.white, fadeSpeed, false));
			for (int i = 0; i < 150; i++) {//three seconds
				var streamClone = UnityEngine.Object.Instantiate(WindStream, sender.transform.position, new Quaternion(0, 0, 0, 1));
				//stream.transform.localScale = sender.transform.localScale;
				streamClone.SetActive(true);
				var streamSub = streamClone.GetComponentInChildren<SpriteRenderer>();

				//var rotationVector = sender.transform.position - reciever.transform.position;
				streamSub.transform.rotation = new Quaternion(0, 0, Vector2.Angle(new Vector2(0, 1), new Vector2(reciever.X, reciever.Y)), 1);

				StartCoroutine(StreamAnimation(streamClone, reciever.Coordinates, 10));
				yield return new WaitForSeconds(Fastest);
				if (i == 15) {
					recieverPulseClone = Instantiate(WindPulse, reciever.transform.position, new Quaternion(0, 0, 0, 1));
				}
			}
			sender.IsRevealed = true;
			reciever.IsRevealed = true;
			yield return new WaitForSeconds(Normal);
			sender.CurrentColor = sender.OriginalColor;
			reciever.CurrentColor = reciever.OriginalColor;
			Destroy(senderPulseClone);
			Destroy(recieverPulseClone);
			foreach (var needsDestroying in toBeDestroyed) {
				Destroy(needsDestroying);
			}
		}

		public IEnumerator SendStonePulse(Stone sender, Stone reciever) {
			var senderPulseClone = Instantiate(WindPulse, sender.transform.position, new Quaternion(0, 0, 0, 1));
			GameObject recieverPulseClone = Instantiate(WindPulse, reciever.transform.position, new Quaternion(0, 0, 0, 1));
			float fadeSpeed = .05f;
			StartCoroutine(FadeStone(sender, sender.OriginalColor, Color.white, fadeSpeed, false));
			StartCoroutine(FadeStone(reciever, reciever.OriginalColor, Color.white, fadeSpeed, false));
			float direction = 0;
			senderPulseClone.transform.localScale = new Vector2(3, 3);
			for (int i = 0; i < 50; i++) {
				direction = i % 10 == 0 ? Fast : Fast * -1;
				float scale = senderPulseClone.transform.localScale.x + direction;
				senderPulseClone.transform.localScale = new Vector3(scale, scale, 1);
				recieverPulseClone.transform.localScale = new Vector3(scale, scale, 1);
				yield return new WaitForSecondsRealtime(Fastest);
			}
			//for (int i = 0; i < 50; i++) {
			//	float scale = senderPulseClone.transform.localScale.x - .03f;
			//	senderPulseClone.transform.localScale = new Vector3(scale, scale, 1);
			//	recieverPulseClone.transform.localScale = new Vector3(scale, scale, 1);
			//	new WaitForSecondsRealtime(Fastest);
			//}
			sender.IsRevealed = true;
			reciever.IsRevealed = true;
			sender.CurrentColor = sender.OriginalColor;
			reciever.CurrentColor = reciever.OriginalColor;
			Destroy(senderPulseClone);
			Destroy(recieverPulseClone);
		}

		public IEnumerator StreamAnimation(GameObject obj, (int x, int y) cell, int speed = 50) {
			var position = Board.GetCellCenterWorld(new Vector3Int(cell.x, cell.y, 0));

			var xVector = position.x - obj.transform.position.x;
			var yVector = position.y - obj.transform.position.y;
			var xStep = xVector / speed;
			var yStep = yVector / speed;

			for (int i = 0; i < speed; i++) {
				obj.transform.Translate(new Vector3(xStep, yStep));
				yield return new WaitForSecondsRealtime(Fastest);
			}
			yield return new WaitForSecondsRealtime(Fast * 3); //Makes it slower than fast.
			Destroy(obj);
		}

		public IEnumerator MoveObjectToCell(GameObject obj, (int x, int y) cell, int speed = 50) {
			numMovingObjects++;
			var position = Board.GetCellCenterWorld(new Vector3Int(cell.x, cell.y, 0));

			var xDistance = position.x - obj.transform.position.x;
			var yDistance = position.y - obj.transform.position.y;
			var xStep = xDistance / speed;
			var yStep = yDistance / speed;

			for (int i = 0; i < speed; i++) {
				obj.transform.Translate(new Vector3(xStep, yStep));
				yield return new WaitForSecondsRealtime(Fastest);
			}
			obj.transform.position = new Vector3(position.x, position.y, obj.transform.position.z);
			numMovingObjects--;
		}

		public IEnumerator MoveGameTokenToCell(GameToken token, (int x, int y) cell) {
			yield return MoveObjectToCell(token.gameObject, cell);
			token.Coordinates = cell;
		}

		public IEnumerator MoveStoneToCell(Stone stone, (int x, int y) cell) {
			yield return MoveGameTokenToCell(stone, cell);
			UnselectStone(stone, cell);
		}

		//Moves a given Stone to the given tile, then unselects.
		public IEnumerator MoveStoneToTile(Stone stone, BoardTile tile) {
			PlaceStoneOnTile(stone, tile);
			yield return MoveStoneToCell(stone, (tile.X, tile.Y));
			EnableActiveStone(stone, tile.Coordinates);
		}

		public IEnumerator MoveStoneToTileFromTile(Stone stone, BoardTile tile, BoardTile start) {
			RemoveStoneFromTile(stone, start);
			yield return MoveStoneToTile(stone, tile);
		}

		private void RemoveStoneFromTile(Stone stone, BoardTile start) {
			stone.UnSelect();
			start.UnSelect();
			start.RemoveStone();
			SelectedTile = null;
		}

		public IEnumerator MoveStoneToCellFromTile(Stone stone, (int x, int y) cell, BoardTile start) {
			RemoveStoneFromTile(stone, start);
			yield return MoveGameTokenToCell(stone, cell);
		}

		public IEnumerator SwapStoneLocations(Stone one, Stone two) {
			var oneCoor = (one.X, one.Y);
			var twoCoor = (two.X, two.Y);
			StartCoroutine(MoveStoneToTile(one, Tiles[twoCoor.X, twoCoor.Y]));
			yield return MoveStoneToTile(two, Tiles[oneCoor.X, oneCoor.Y]);
			one.Coordinates = twoCoor;
			two.Coordinates = oneCoor;
		}

		public IEnumerator MoveWellToCell(Well well, (int x, int y) cell) {
			yield return MoveGameTokenToCell(well, cell);
		}

		public IEnumerator MoveWellToTile(Well well, BoardTile tile) {
			yield return MoveWellToCell(well, (tile.X, tile.Y));
			PlaceWellOnTile(well, tile);

		}

		public IEnumerator MoveWellToTileFromTile(Well well, BoardTile tile, BoardTile start) {
			RemoveWellFromTile(well, start);
			yield return MoveWellToTile(well, tile);
		}

		public IEnumerator MoveWellToCellFromTile(Well well, (int x, int y) cell, BoardTile start) {
			RemoveWellFromTile(well, start);
			yield return MoveWellToCell(well, cell);

		}

		private int _moveWellToStoneThenCellFromTileCount = 0;
		public bool MoveWellToStoneThenCellFromTileIsRunning => _moveWellToStoneThenCellFromTileCount != 0;
		public IEnumerator MoveWellToStoneThenCellFromTile(Well well, Stone stone, (int x, int y) cell, BoardTile start) {
			_moveWellToStoneThenCellFromTileCount++;
			yield return MoveWellToCellFromTile(well, stone.Coordinates, start);
			new WaitForSecondsRealtime(Normal);
			yield return MoveWellToCell(well, cell);
			new WaitForSecondsRealtime(Normal);
			_moveWellToStoneThenCellFromTileCount--;

			if (_moveWellToStoneThenCellFromTileCount == 0 && NoWellInPlay) {
				yield return GameManager.Game.ResetWells();
			}
		}

		public IEnumerator AlwaysCheckWellState() {
			while (true) {
				yield return null;
				if (SelectedStone == null) {
					continue;
				}
				else if (SelectedStone.InPlay == false) {
					IndicateWellNear();
				}
				else {
					foreach (var tile in Tiles) {
						tile.IndicateColor(true, false);
					}
				}
			}
		}

		public IEnumerator WaitThenUnselectTile(BoardTile tile) {
			yield return new WaitForSecondsRealtime(Slow * 3);
			RemoveSelection();
			tile.UnSelect();
			SelectedTile = null;
		}

		public IEnumerator ShowCellAsSelected((int x, int y) cell, Color color) {
			//size and position of selection icon
			float scale = 2f;

			Selection.transform.localScale = new Vector3Int((int)scale, (int)scale, 0);
			Selection.transform.position = Board.GetCellCenterWorld(new Vector3Int(cell.x, cell.y, 0));
			Selection.GetComponent<SpriteRenderer>().color = color;
			Selection.SetActive(true);

			for (int i = 0; i <= 10; i++) {
				Selection.transform.localScale = new Vector3(scale, scale, 0);
				scale -= .1f;
				yield return new WaitForSecondsRealtime(Fastest);
			}
		}

		public IEnumerator RemoveSelection() {
			ClearIndicators();
			SelectedStone?.UnSelect();
			SelectedStone = null;
			SelectedTile?.UnSelect();
			SelectedTile = null;

			//size and position of selection icon
			float scale = 1;
			for (int i = 0; i <= 10; i++) {
				Selection.transform.localScale = new Vector3(scale, scale, 0);
				scale += .1f;
				yield return new WaitForSecondsRealtime(Fastest);
			}

			Selection.SetActive(false);
		}

		public IEnumerator MoveSelectionFromCellToCell((int x, int y) cell, Color color) {
			RemoveSelection();
			yield return MoveObjectToCell(Selection, cell);
			yield return ShowCellAsSelected(cell, color);
		}

		public IEnumerator WaitThenUnselectStone(Stone stone) {
			yield return new WaitForSecondsRealtime(Slow * 3);
			RemoveSelection();
			stone.UnSelect();
			SelectedStone = null;
		}

		public IEnumerator ShuffleStonesUpdward((int x, int y) gapToFill, List<GameObject> stones) {
			foreach (var stone in stones.OrderBy(x => x.GetComponent<Stone>().Y).Reverse()) {
				var controller = stone.GetComponent<Stone>();
				if (!controller.InPlay && controller.Y < gapToFill.y) {
					(int x, int y) newGapToFill = (controller.X, controller.Y);
					yield return MoveStoneToCell(controller, gapToFill);
					gapToFill = newGapToFill;
				}
			}
		}

		internal IEnumerator RevealLodenStone(List<Stone> stones) {
			GameManager.Game.HideStones = false;

			float step = .01f;
			var unrevealedStones = stones.Where(s => s.IsRevealed == false);
			foreach (var stone in unrevealedStones) {
				StartCoroutine(FadeStone(stone, stone.OriginalColor, Color.white, step, false));
			}
			yield return new WaitForSecondsRealtime(Slow * 3);
			foreach (var stone in unrevealedStones) {
				StartCoroutine(FadeStone(stone, stone.OriginalColor, Color.white, step, true));
			}
			yield return new WaitForSecondsRealtime(Slow);

			GameManager.Game.HideStones = true;
		}

		/// <summary>
		/// Fades the stone's color to the color given
		/// </summary>
		/// <param name="stone"></param>
		/// <param name="light"></param>
		/// <param name="step">step must move the stone color toward the passed in color is this function won't work</param>
		/// <returns></returns>
		public IEnumerator FadeStone(Stone stone, Color dark, Color light, float step, bool darkToLight) {
			if (stone.IsRevealed == false) {
				float dr = dark.r;
				float dg = dark.g;
				float db = dark.b;
				float lr = light.r;
				float lg = light.g;
				float lb = light.b;
				bool close = false;

				step = darkToLight ? Math.Abs(step) * -1 : Math.Abs(step); // insures we go the right direction
				while (close == false) {
					close = true;
					if (lr > dr) {
						if (darkToLight) {
							dr -= step;
						}
						else {
							lr -= step;
						}
						close = false;
					}
					if (lg > dg) {
						if (darkToLight) {
							dg -= step;
						}
						else {
							lg -= step;
						}
						close = false;
					}
					if (lb > db) {
						if (darkToLight) {
							db -= step;
						}
						else {
							lb -= step;
						}
						close = false;
					}
					if (darkToLight) {
						stone.CurrentColor = new Color(dr, dg, db);
					}
					else {
						stone.CurrentColor = new Color(lr, lg, lb);
					}
					yield return new WaitForSecondsRealtime(Fast);
				}
				stone.ColorIndicator.color = darkToLight ? light : dark;
			}

		}


		#endregion

		#region Instantatious Placement

		public void InstantlyMoveObjectToCell(GameObject obj, (int x, int y) cell, int speed = 50) {
			var position = Board.GetCellCenterWorld(new Vector3Int(cell.x, cell.y, 0));
			obj.transform.position = new Vector3(position.x, position.y, obj.transform.position.z);
		}

		public void InstantlyMoveGameTokenToCell(GameToken token, (int x, int y) cell) {
			InstantlyMoveObjectToCell(token.gameObject, cell);
			token.Coordinates = cell;
		}

		public void InstantlyMoveStoneToCell(Stone stone, (int x, int y) cell) {
			InstantlyMoveGameTokenToCell(stone, cell);
			UnselectStone(stone, cell);
		}

		//Moves a given Stone to the given tile, then unselects.
		public void InstantlyMoveStoneToTile(Stone stone, BoardTile tile) {
			PlaceStoneOnTile(stone, tile);
			InstantlyMoveStoneToCell(stone, (tile.X, tile.Y));
			EnableActiveStone(stone, tile.Coordinates);
		}

		public void InstantlyMoveStoneToTileFromTile(Stone stone, BoardTile tile, BoardTile start) {
			RemoveStoneFromTile(stone, start);
			InstantlyMoveStoneToTile(stone, tile);
		}

		public void InstantlyMoveStoneToCellFromTile(Stone stone, (int x, int y) cell, BoardTile start) {
			RemoveStoneFromTile(stone, start);
			InstantlyMoveGameTokenToCell(stone, cell);
		}

		public void InstantlyMoveWellToCell(Well well, (int x, int y) cell) {
			InstantlyMoveGameTokenToCell(well, cell);
		}

		public void InstantlyMoveWellToTile(Well well, BoardTile tile) {
			InstantlyMoveWellToCell(well, (tile.X, tile.Y));
			PlaceWellOnTile(well, tile);
		}

		public void InstantlyMoveWellToTileFromTile(Well well, BoardTile tile, BoardTile start) {
			RemoveWellFromTile(well, start);
			InstantlyMoveWellToTile(well, tile);
		}

		public void InstantlyMoveWellToCellFromTile(Well well, (int x, int y) cell, BoardTile start) {
			RemoveWellFromTile(well, start);
			InstantlyMoveWellToCell(well, cell);

		}


		#endregion
	}
}